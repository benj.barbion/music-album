<?php

namespace Database\Seeders;

use App\Models\Artist;
use Illuminate\Database\Seeder;

class ArtistsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artist::findOrFail(1)->update(['pochette' => 'artists/LRPN2YgwRMIdHvZ9W10Dc8aerEaGUrNsn6vDSTli.jpg']);
        Artist::updateOrCreate(['name' => 'Benjamin'], ['name' => 'Benjamin', 'pochette' => 'artists/ViiR2Il27ZvbsPclX8uw5TzWnuinyWX3pdA9p5K1.png']);
    }
}
