<?php

namespace App\Http\Controllers;

use App\Models\Album;

class IndexController extends Controller
{
    public function index(){
        return view('index')->with('albums', Album::popular(8)->get());
    }
}
