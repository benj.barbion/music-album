@unless(auth()->user())
    <section class="pt-20 pb-24 bg-blue-600">
        <div class="container px-4 mx-auto">
            <div class="max-w-xl mx-auto text-center">
                <span class="inline-block text-xs py-1 px-3 bg-blue-500 text-white font-semibold rounded-xl">ALBUM APP</span>
                <h2 class="my-3 text-3xl md:text-4xl text-white font-bold font-heading">
                    Envie de découvrir notre catalogue complet ?
                </h2>
                <p class="text-sm md:text-base text-white">Merci de vous connecter ou de vous inscrire.</p>
                <div class="mt-10">
                    <a class="inline-block py-4 px-8 text-xs text-blue-600 hover:text-white font-semibold leading-none bg-white hover:bg-blue-600 border hover:border-white rounded transition duration-300"
                       href="{{route('register')}}">INSCRIPTION</a></div>
            </div>
        </div>
    </section>
@endunless
