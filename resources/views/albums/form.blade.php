<input value="{{$album->title ?? old('title')}}" class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('title') border-2 border-red-500 @enderror" type="text" name="title" placeholder="Tire de l'album" required>
@error('title')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

<select class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none" name="artist_id" required>
    <option value="0">--Sélectionner l'artiste --</option>
    @foreach($artists as $artist)
        <option value="{{$artist->id}}" {{$album->artist?->id == $artist->id ? 'selected' : ''}}>{{$artist->name}}</option>
    @endforeach
</select>
@error('artist')<p class="text-red-500 text-xs">{{$message}}</p>@enderror

<span class="font-medium block mt-4">Pochette:</span>
<input class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none" type="file" name="pochette">

<button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
