<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Commande <span class="text-blue-600">{{$invoice->id}}</span>
        </x-slot>
        <a href="{{route('invoices.index')}}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Liste des commandes
        </a>
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">Titre</th>
                <th scope="col" class="px-6 py-3">Artiste</th>
                <th scope="col" class="px-6 py-3">Quantité</th>
                <th scope="col" class="px-6 py-3">Prix unitaire</th>
                <th scope="col" class="px-6 py-3">Prix total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice->invoicesItems as $line)
                <tr class="bg-white border-b">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{$line->track?->name}}</th>
                    <td class="px-6 py-4">{{$line->track->album?->artist?->name ?? 'Artiste inconnu'}}</td>
                    <td class="px-6 py-4">{{$line->quantity}}</td>
                    <td class="px-6 py-4">{{$line->unit_price}}Є</td>
                    <td class="px-6 py-4">{{$line->unit_price*$line->quantity}}Є</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</x-layout>


