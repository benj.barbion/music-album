<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Customer extends Model
{
    use HasFactory, Sortable, SoftDeletes;

    protected $fillable = ['firstname', 'lastname',  'company',  'address', 'city', 'state', 'country', 'postalcode', 'phone', 'fax', 'email', 'employee_id'];
    public $sortable = ['id', 'firstname', 'lastname', 'email', 'phone', 'city', 'country'];

    public function employee(): BelongsTo {
        return $this->belongsTo(Customer::class, 'employee_id');
    }

    public function invoices(): HasMany {
        return $this->hasMany(Invoice::class, 'customer_id');
    }

    public function fullName() {
        return $this->lastname . ' ' . $this->firstname;
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['search'] ?? false)
            return $query->where('lastname', 'like', '%' . $filter['search'] . '%')
                         ->orWhere('firstname', 'like', '%' . $filter['search'] . '%')
                         ->orWhere('company', 'like', '%' . $filter['search'] . '%')
                         ->orWhere('city', 'like', '%' . $filter['search'] . '%')
                         ->orWhere('postalcode', '=', $filter['search'])
                         ->orWhere('email', 'like', '%' . $filter['search'] . '%');
        return $query;
    }
}
