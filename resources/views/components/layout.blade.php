<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Album APP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Benjamin">
    <link rel="stylesheet" href="{{asset('/css/tailwind/tailwind.min.css')}}">
    <link rel="icon" sizes="16x16" href="{{asset('images/favicon.png?')}}">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
</head>
<body class="antialiased bg-body text-body font-body">
<x-navbar />
<x-hero />
<main class="bg-blueGray-50 py-10">
    <section>
        <div class="container px-4 mx-auto">
            <div class="flex flex-wrap lg:flex-nowrap bg-white shadow rounded">
                <div class="w-full md:w-1/2 lg:w-1/4 px-4 py-6 text-center border-b lg:border-b-0 md:border-r">
                    <p class="text-md lg:text-base text-blueGray-400">
                        <span>Total des albums</span>
                    </p>
                    <p class="my-1 text-3xl lg:text-4xl font-bold font-heading"><a href="{{route('albums.index')}}">{{$stats->userCount}}</a></p>
                </div>
                <div class="w-full md:w-1/2 lg:w-1/4 px-4 py-6 text-center border-b lg:border-b-0 md:border-r">
                    <p class="text-md lg:text-base text-blueGray-400">
                        <span>Total des playlists</span>
                    </p>
                    <p class="my-1 text-3xl lg:text-4xl font-bold font-heading"><a href="{{route('playlists.index')}}">{{$stats->playlistCount}}</a></p>
                </div>
                <div class="w-full md:w-1/2 lg:w-1/4 px-4 py-6 text-center border-b md:border-b-0 md:border-r">
                    <p class="text-md lg:text-base text-blueGray-400">
                        <span>Total des artists</span>
                    </p>
                    <p class="my-1 text-3xl lg:text-4xl font-bold font-heading"><a href="{{route('artists.index')}}">{{$stats->artistCount}}</a></p>
                </div>
                <div class="w-full md:w-1/2 lg:w-1/4 px-4 py-6 text-center">
                    <p class="text-md lg:text-base text-blueGray-400">
                        <span>Total des titres</span>
                    </p>
                    <p class="my-1 text-3xl lg:text-4xl font-bold font-heading"><a href="{{route('tracks.index')}}">{{$stats->trackCount}}</a></p>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-10">
        <div class="container px-4 mx-auto">
            <x-flash />
            {{$slot}}
        </div>
    </section>
</main>
<section class="py-16">
    <div class="container px-4 mx-auto">
        <div class="flex flex-col lg:flex-row mb-10">
            <a class="inline-block mx-auto mb-10 lg:mb-0 lg:ml-0 lg:mr-auto text-3xl font-semibold leading-none" href="#">
                <img src="{{asset('/images/logo.png')}}" alt="" width="auto">
            </a>
            <ul class="flex lg:flex-row items-center justify-center space-x-12">
                    <li><a class="text-lg font-bold font-heading hover:text-blueGray-600"href="{{route('albums.index')}}">Albums</a></li>
                <li><a class="text-lg font-bold font-heading hover:text-blueGray-600" href="{{route('playlists.index')}}">Playlists</a></li>
                <li><a class="text-lg font-bold font-heading hover:text-blueGray-600" href="{{route('artists.index')}}">Artistes</a></li>
                <li><a class="text-lg font-bold font-heading hover:text-blueGray-600" href="{{route('tracks.index')}}">Titres</a></li>
            </ul>
        </div>
        <div class="flex flex-col lg:flex-row items-center lg:justify-between">
            <p class="text-xs text-blueGray-400">© 2022 All rights reserved.</p>
            <div class="order-first lg:order-last -mx-2 mb-4 lg:mb-0">
                <a class="inline-block px-2" href="#">
                </a>
                <a class="inline-block px-2" href="#">
                </a>
                <a class="inline-block px-2" href="#">
                </a>
            </div>
        </div>
    </div>
</section>
<script src="{{asset('/js/main.js')}}"></script>
<script src="https://unpkg.com/flowbite@1.4.7/dist/flowbite.js"></script>
</body>
</html>
