<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create permissions
        Permission::create(['name' => 'cart']);

        Permission::create(['name' => 'employees.*']);
        Permission::create(['name' => 'employees.edit']);
        Permission::create(['name' => 'employees.delete']);
        Permission::create(['name' => 'employees.create']);
        Permission::create(['name' => 'employees.show']);

        Permission::create(['name' => 'albums.*']);
        Permission::create(['name' => 'albums.edit']);
        Permission::create(['name' => 'albums.delete']);
        Permission::create(['name' => 'albums.create']);

        Permission::create(['name' => 'artists.*']);
        Permission::create(['name' => 'artists.edit']);
        Permission::create(['name' => 'artists.delete']);
        Permission::create(['name' => 'artists.create']);

        Permission::create(['name' => 'invoices.*']);
        Permission::create(['name' => 'invoices.show']);
        Permission::create(['name' => 'invoices.delete']);

        Permission::create(['name' => 'customers.*']);
        Permission::create(['name' => 'customers.edit']);
        Permission::create(['name' => 'customers.delete']);
        Permission::create(['name' => 'customers.create']);
        Permission::create(['name' => 'customers.show']);

        Permission::create(['name' => 'playlists.*']);
        Permission::create(['name' => 'playlists.delete']);
        Permission::create(['name' => 'playlists.create']);

        Permission::create(['name' => 'roles.*']);
        Permission::create(['name' => 'roles.delete']);
        Permission::create(['name' => 'roles.create']);
        Permission::create(['name' => 'roles.edit']);
        Permission::create(['name' => 'roles.show']);

        Permission::create(['name' => 'tracks.*']);
        Permission::create(['name' => 'tracks.edit']);
        Permission::create(['name' => 'tracks.delete']);
        Permission::create(['name' => 'tracks.create']);

        // Create roles
        $superadmin = Role::create(['name' => 'super admin']);
        $generalManager = Role::create(['name' => 'general manager']);
        $saleManager = Role::create(['name' => 'sales manager']);
        $saleSupportAgent = Role::create(['name' => 'sales support agent']);

        // Give permissions to roles
        $generalManager->givePermissionTo('employees.*');
        $generalManager->givePermissionTo('albums.*');
        $generalManager->givePermissionTo('artists.*');
        $generalManager->givePermissionTo('cart');
        $generalManager->givePermissionTo('invoices.*');
        $generalManager->givePermissionTo('playlists.*');
        $generalManager->givePermissionTo('tracks.*');
        $generalManager->givePermissionTo('customers.*');

        $saleManager->givePermissionTo('customers.*');
        $saleManager->givePermissionTo('albums.*');
        $saleManager->givePermissionTo('cart');
        $saleManager->givePermissionTo('invoices.show');
        $saleManager->givePermissionTo('tracks.create');

        $saleSupportAgent->givePermissionTo('cart');
        $saleSupportAgent->givePermissionTo('invoices.show');
        $saleSupportAgent->givePermissionTo('customers.show');

        // Special user (super admin)
        $user = User::factory()->create([
            'name' => 'Benjamin',
            'email' => 'b.barbion@gmail.com',
            'password' => bcrypt('Benjamin')
        ]);
        $user->assignRole($superadmin);

        $this->createUser('Adams', 'andrew@chinookcorp.com', $generalManager, 1);
        $this->createUser('Edwards', 'nancy@chinookcorp.com', $saleManager, 2);
        $this->createUser('Peacock', 'jane@chinookcorp.com', $saleSupportAgent, 3);
        $this->createUser('Park', 'margaret@chinookcorp.com', $saleSupportAgent, 4);

        // Lambda user (wish user)
        User::factory()->create([
            'name' => 'René',
            'email' => 'r.rene@gmail.com',
            'password' => bcrypt('René')
        ]);
    }

    private function createUser($name, $email, $role, $idDb){
        $user = User::factory()->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($name)
        ]);
        $user->assignRole($role);
        $userDb = Employee::find($idDb);
        $userDb->user_id = $user->id;
        $userDb->save();
    }
}
