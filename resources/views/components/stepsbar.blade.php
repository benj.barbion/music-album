@props(['id'])
<div class="container px-4 mx-auto text-center">
    <div class="max-w-lg mx-auto mb-10 pt-2">
        <span class="inline-block py-1 px-3 text-xs font-semibold bg-blue-100 text-blue-600 rounded-xl">Les commandes</span>
        <h2 class="text-3xl md:text-4xl mt-2 mb-4 font-bold font-heading">Pour vos clients SIIIIII fabuleux.</h2>
        <p class="text-blueGray-400 leading-loose">Ajouter une commande en 3 étapes !</p>
    </div>
    <div class="flex relative">
        <div class="hidden md:flex absolute inset-0 -mt-24 px-16 items-center justify-center max-w-4xl mx-auto">
            <div class="w-full py-1 shadow"></div>
        </div>
        <div class="flex flex-wrap justify-between w-full">
            <div class="w-1/2 md:w-1/4 z-10 mb-12">
                <div class="flex w-20 h-20 mx-auto items-center justify-center shadow bg-white rounded-full">
                    <div class="flex w-16 h-16 mx-auto items-center justify-center font-semibold rounded-full @if($id == 1) text-white bg-blue-600 @else  text-blueGray-400 bg-blueGray-50 @endif ">1</div>
                </div>
                <p class="mt-6 text-base lg:text-lg @if($id != 1) text-blueGray-400 @endif font-bold font-heading text-center">Choisissez le client</p>
            </div>
            <div class="w-1/2 md:w-1/4 z-10 mb-12">
                <div class="flex w-20 h-20 mx-auto items-center justify-center shadow bg-white rounded-full">
                    <div class="flex w-16 h-16 mx-auto items-center justify-center font-semibold rounded-full @if($id == 2) text-white bg-blue-600 @else  text-blueGray-400 bg-blueGray-50 @endif ">2</div>
                </div>
                <p class="mt-6 text-base lg:text-lg @if($id != 2) text-blueGray-400 @endif font-bold font-heading text-center">Choisissez les articles</p>
            </div>
            <div class="w-1/2 md:w-1/4 z-10 mb-12">
                <div class="flex w-20 h-20 mx-auto items-center justify-center shadow bg-white rounded-full">
                    <div class="flex w-16 h-16 mx-auto items-center justify-center font-semibold rounded-full @if($id == 3) text-white bg-blue-600 @else  text-blueGray-400 bg-blueGray-50 @endif ">3</div>
                </div>
                <p class="mt-6 text-base lg:text-lg @if($id != 3) text-blueGray-400 @endif font-bold font-heading text-center">Confirmation commande</p>
            </div>
        </div>
    </div>
</div>
