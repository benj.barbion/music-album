<x-layout>
    <x-title>
        <x-slot name="pageHeading">
            Création d'un <span class="text-blue-600">client</span>
        </x-slot>
        <a href="{{ route('customers.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour
        </a>
    </x-title>
    <section class="pt-10">
        <div class="container px-4 mx-auto">
            <div class="flex max-w-lg mx-auto flex-col">
                <div class="p-8 bg-white rounded shadow">
                    <div class=" text-center">
                        <span class="text-sm text-blueGray-400 content-center">Clients</span>
                        <h4 class="mb-6 text-3xl">Créer un client</h4>
                    </div>
                    <form method="POST" action="{{route('customers.store')}}" role="form" enctype="multipart/form-data">
                        @csrf
                        @include('customers.form')
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-layout>
