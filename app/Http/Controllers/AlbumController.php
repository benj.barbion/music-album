<?php

namespace App\Http\Controllers;

use App\Exports\AlbumsExport;
use App\Exports\AlbumWithDetailsExport;
use App\Models\Album;
use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class AlbumController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:albums.edit')->only(['edit', 'update']);
        $this->middleware('permission:albums.delete')->only(['destroy', 'restore']);
        $this->middleware('permission:albums.create')->only(['create', 'store']);
    }

    public function index(){
        $datas = auth()->user()->can('albums.delete') ? Album::withTrashed() : Album::withoutTrashed();

        $datas = $datas->sortable()
            ->withCount('tracks as tracks_count')
            ->filter(\request()->input())
            ->orderBy('albums.updated_at', 'desc')
            ->paginate(15);

        return view('albums.index')->with('albums', $datas);
    }

    public function show(Album $album){
        $tracks = auth()->user()->can('albums.delete') ? $album->tracks()->withTrashed() :  $album->tracks()->withoutTrashed();
        $tracks = $tracks->orderBy('id', 'desc')->paginate(15);
        return view('albums.show', ['album' => $album, 'tracks' => $tracks]);
    }

    public function restore($idAlbum){
        $album = Album::onlyTrashed()->findOrFail($idAlbum);
        $album->restore();
        return back()->with('success', 'Album restauré avec succès.');
    }

    public function create(){
        return view('albums.create')
            ->with('artists', Artist::orderBy('name', 'asc')->get(['id', 'name']))
            ->with('album', new Album);
    }

    public function store(Request $request){
        $formFields = $request->validate([
            'artist_id' => ['required', Rule::exists('albums', 'artist_id')],
            'title' => ['required', Rule::unique('albums', 'title')->where(function($query) {
                return $query->where('artist_id', \request('artist_id'));
            })]
        ]);

        if ($request->hasFile('pochette'))
            $formFields['pochette'] = $request->file('pochette')->store('pochettes', 'public');

        Album::create($formFields);

        return back()->with('success', 'Création réussie avec succès !');
    }

    public function edit(Album $album){
        return view('albums.edit')
            ->with('artists', Artist::orderBy('name', 'asc')->get(['id', 'name']))
            ->with('album', $album);
    }

    public function update(Request $request, Album $album){
        $formFields = [];

        if ($request->input('artist_id') != $album->artist_id || $request->input('title') != $album->title) {
            $formFields = $request->validate([
                'artist_id' => ['required', Rule::exists('albums', 'artist_id')],
                'title' => ['required', Rule::unique('albums', 'title')->where(function($query) {
                    return $query->where('artist_id', \request('artist_id'));
                })]
            ]);
        }

        if ($request->hasFile('pochette'))
            $formFields['pochette'] = $request->file('pochette')->store('pochettes', 'public');

        $album->update($formFields);

        return back()->with('success', 'Modification réussie avec succès !');
    }

    public function destroy(Album $album){
        $album->delete();
        return back()->with('success', 'Suppression de l\'album avec succès !');
    }

    public function export(){
        return Excel::download(new AlbumsExport, 'albums.xlsx');
    }

    public function exportOne(Album $album){
        return Excel::download(new AlbumWithDetailsExport($album), 'album_'.$album->title.'.xlsx');
    }

    public function addToCart(Album $album){
        $tracks = $album->tracks;
        foreach ($tracks as $track)
            \Gloudemans\Shoppingcart\Facades\Cart::add($track, 1,  ['artiste' => $track->artist]);
        return back()->with('success', 'Ajout au panier avec succès !');
    }
}
