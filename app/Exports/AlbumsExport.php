<?php

namespace App\Exports;

use App\Models\Album;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class AlbumsExport implements FromView, ShouldAutoSize, WithColumnWidths
{
    use Exportable;

    public function view(): View
    {
        return view('exports.albums', [
            'albums' => Album::withoutTrashed()->withCount('tracks')->get()
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 55,
            'B' => 45,
        ];
    }
}
