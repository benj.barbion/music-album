<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Création d'un <span class="text-blue-600">artiste</span>
        </x-slot>
        <a href="{{ route('artists.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour aux artistes
        </a>
    </x-title>

    <div class="flex max-w-lg mx-auto flex-col mt-10">
        <div class="p-8 bg-white rounded shadow">
            <div class=" text-center">
                <span class="text-sm text-blueGray-400 content-center">Artistes</span>
                <h4 class="mb-6 text-3xl">Créer un artiste</h4>
            </div>
            <form method="POST" action="{{ route('artists.store') }}" role="form" enctype="multipart/form-data">
                @csrf
                @include('artists.form')
            </form>
        </div>
    </div>
</x-layout>
