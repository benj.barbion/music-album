<?php

namespace Database\Seeders;

use App\Models\Album;
use App\Models\InvoiceItem;
use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Album::findOrFail(23)->update(['pochette' => 'pochettes/1il4ubu6nKDP8T8V3moKcQQIze1LRlfunqjftVp5.jpg']);
        Album::findOrFail(221)->update(['pochette' => 'pochettes/cUMbNdecKzft21ertel1IWieNBcGeB8jaVL2L6UB.jpg']);
        Album::findOrFail(73)->update(['pochette' => 'pochettes/eJXwlXMPC5By1wTuR9a5BLC7rRaWVwen79d0Bvuv.jpg']);
        Album::findOrFail(141)->update(['pochette' => 'pochettes/ezsOqTTKfVtNaJCNlJc54DDrUTangadgtH7I7U6l.jpg']);
        Album::findOrFail(55)->update(['pochette' => 'pochettes/ksTcPWV38OofnwiBWXnoi7Sv05UTc8V6yU3d4B7a.jpg']);
        Album::findOrFail(224)->update(['pochette' => 'pochettes/pHC12eTWOsEAdjmUUG6todxPzMd4PsNGEtzp3WIX.jpg']);
        Album::findOrFail(37)->update(['pochette' => 'pochettes/WQigvE1QJsRH7ooFZQQP4Vdy8cKj0Kj1sQNj17nq.jpg']);
        Album::findOrFail(21)->update(['pochette' => 'pochettes/V2jV1TLx3KNayw03A1WS3B5oCp0u5pJuk9tfjWQF.jpg']);

        Album::updateOrCreate(['artist_id' => 52, 'title' => 'Destroyers'], ['artist_id' => 52, 'title' => 'Destroyers', 'pochette' => 'pochettes/17SeKLPJTvl6SbBc92oBXPwC0e7MmtRjldl3yxX2.jpg']);
        Album::updateOrCreate(['artist_id' => 2,  'title' => 'Balls to the wall'], ['artist_id' => 2,  'title' => 'Balls to the wall', 'pochette' => 'pochettes/jXRmF2hKbLJeapo0rjVmvRCANuPyZ8ZqDOYwb34F.jpg']);
        Album::updateOrCreate(['artist_id' => 1,  'title' => 'Rock and roll'], ['artist_id' => 1,  'title' => 'Rock and roll', 'pochette' => 'pochettes/PfAzRIV6GmU8wtVobhVN8bIHPKWdQ7jmZ0Q8eCjm.jpg']);
        Album::updateOrCreate(['artist_id' => 51, 'title' => 'A kind of magic'], ['artist_id' => 51, 'title' => 'A kind of magic', 'pochette' => 'pochettes/xOyibfKS0JnfF6zDpJWJFYbKp3pH9gm5ihupQGs2.png']);

        InvoiceItem::findOrFail(39)->update(['quantity' => 56]);
        InvoiceItem::findOrFail(859)->update(['quantity' => 6]);
        InvoiceItem::findOrFail(726)->update(['quantity' => 104]);
    }
}
