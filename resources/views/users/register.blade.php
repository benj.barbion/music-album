<x-layout>
    <x-title>
        <x-slot name="pageHeading">
            Création d'un <span class="text-blue-600">compte</span>
        </x-slot>
        <a href="{{ route('index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour à l'accueil
        </a>
    </x-title>
    <section class="pt-10">
        <div class="container px-4 mx-auto">
            <div class="flex max-w-lg mx-auto flex-col">
                <div class="p-8 bg-white rounded shadow">
                    <div class=" text-center">
                        <span class="text-sm text-blueGray-400 content-center">Utilisateur</span>
                        <h4 class="mb-6 text-3xl">Créer un compte</h4>
                    </div>
                    <form method="POST" action="{{ route('users.store') }}" role="form" enctype="multipart/form-data">
                        @csrf

                        <input
                            value="{{$user->name}}"
                            type="text" name="name" placeholder="Nom"
                            class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none {{$errors->has('name') ? ' border-2 border-red-500' : ''}}"
                            required>
                        @error('title')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

                        <input
                            value="{{$user->email}}"
                            type="email" name="email" placeholder="Email"
                            class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none {{$errors->has('email') ? ' border-2 border-red-500' : ''}}"
                            required>
                        @error('email')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

                        <input
                            value="{{$user->password}}"
                            type="password" name="password" placeholder="Mot de passe"
                            class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none {{$errors->has('password') ? ' border-2 border-red-500' : ''}}"
                            required>
                        @error('password')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

                        <input
                            type="password" name="password_confirmation" placeholder="Mot de passe"
                            class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none {{$errors->has('password_confirmation') ? ' border-2 border-red-500' : ''}}"
                            required>

                        <button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-layout>


