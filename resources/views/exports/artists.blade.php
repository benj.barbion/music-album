<table>
    <thead>
        <tr>
            <th>Nom</th>
            <th>Nombre d'albums</th>
            <th>Nombre de titres</th>
        </tr>
    </thead>
    <tbody>
        @foreach($artists as $artist)
            <tr>
                <td>{{ $artist->name }}</td>
                <td>{{ $artist->albums_count == 0 ? 'Aucun album' : $artist->albums_count}}</td>
                <td>{{ $artist->tracks_count == 0 ? 'Aucun titre' : $artist->tracks_count}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
