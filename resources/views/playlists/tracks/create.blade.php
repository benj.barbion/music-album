<x-layout>

    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Ajout d'un <span class="text-blue-600">titre</span>
        </x-slot>
        <a href="{{ route('playlists.show', $playlist->id) }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour à la playlist
        </a>
    </x-title>

    <div class="flex max-w-lg mx-auto flex-col mt-10">
        <div class="p-8 bg-white rounded shadow">
            <div class=" text-center">
                <span class="text-sm text-blueGray-400 content-center">Playlists</span>
                <h4 class="mb-6 text-3xl">Ajout d'un titre</h4>
            </div>
            <form method="POST" action="{{route('playlists.tracks.store', $playlist->id)}}" role="form" enctype="multipart/form-data">
                @csrf
                <select class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('track_id') border-2 border-red-500 @enderror" name="track_id" required>
                    <option value="0">--Sélectionner le titre --</option>
                    @foreach($tracks as $track)
                        <option value="{{$track->id}}">{{$track->name}}</option>
                    @endforeach
                </select>
                @error('track_id')<p class="text-red-500 text-xs">{{$message}}</p>@enderror
                <button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">
                    Valider les informations
                </button>
            </form>
        </div>
    </div>
</x-layout>
