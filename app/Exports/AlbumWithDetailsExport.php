<?php

namespace App\Exports;

use App\Models\Album;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AlbumWithDetailsExport implements WithMultipleSheets
{
    private Album $album;

    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    public function sheets(): array
    {
        $sheets = [];

        $this->album->load('tracks'); // eager loading tracks

        $sheets[] = new AlbumExport($this->album);
        $sheets[] = new TracksExport($this->album->tracks);

        return $sheets;
    }
}
