<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaylistTrack extends Model
{
    use HasFactory;

    protected $table = 'playlist_track';
    protected $fillable = ['track_id', 'playlist_id'];

    public function playlist() {
        return $this->belongsTo(Playlist::class, 'playlist_id');
    }

    public function track() {
        return $this->belongsTo(Track::class, 'track_id');
    }
}
