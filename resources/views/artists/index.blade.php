<x-layout>
    {{ Breadcrumbs::render() }}

    <x-search title="des artistes" searchBy="Cherchez par les noms d'artistes." />

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">artistes</span>
        </x-slot>
        <a href="{{ route('artists.export') }}" class="cursor-pointer px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Exporter <i class="ml-2 fa fa-file-excel-o" aria-hidden="true"></i>
        </a>
        @can('artists.create')
            <a href="{{route('artists.create')}}" class="ml-2 cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter un artiste
            </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('name', 'Nom de l\'artiste')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('albums_count', 'Nombre d\'albums')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('tracks_count', 'Nombre de titres')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('updated_at', 'Dernière modification')
                </th>
                <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Actions</span>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($artists as $artist)
                <tr class="bg-white border-b">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap flex items-center">
                        <img alt="pochette" class="w-12 rounded-full hidden lg:flex" src="{{$artist->pochette ? asset('/storage/'.$artist->pochette) : asset('/images/no-image.png')}}">
                        <div class="pl-4">
                            <p class="font-semibold">{{$artist->name}}</p>
                        </div>
                    </th>
                    <td class="px-6 py-4">
                        {{$artist->albums_count}}
                    </td>
                    <td class="px-6 py-4">
                        {{$artist->tracks_count}}
                    </td>
                    <td class="px-6 py-4">
                        {{$artist->updated_at}}
                    </td>
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            @unless($artist->trashed())
                                <a class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('albums.index', ['artist' => $artist->id])}}">🔎</a>

                                @can('artists.edit')
                                    <a class="border ml-1 hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('artists.edit', $artist->id)}}">✏</a>
                                @endcan

                                @can('artists.delete')
                                    <form action="{{route('artists.destroy', $artist->id)}}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">❌</button>
                                    </form>
                                @endcan
                            @else
                                <form action="{{ route('artists.restore', $artist->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                                </form>
                            @endunless
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if($artists->hasPages())
        <div class="mt-10">
            {!! $artists->appends(\Request::except('page'))->render() !!}
        </div>
    @endif
</x-layout>
