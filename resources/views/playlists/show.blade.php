<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Playlist <span class="text-blue-600">{{$playlist->name}}</span>
        </x-slot>

        <a href="{{ route('playlists.export', $playlist->id) }}" class="cursor-pointer px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Exporter <i class="ml-2 fa fa-file-excel-o" aria-hidden="true"></i>
        </a>

        @can('playlists.create')
            <a href="{{ route('playlists.tracks.create', $playlist->id) }}" class="ml-2 cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter un titre
            </a>
        @endcan
        @can('playlists.delete')
            <form action="{{ route('playlists.destroy', $playlist->id) }}" method="POST" class="inline m-0">
                @csrf @method('DELETE')
                <button class="ml-2 cursor-pointer px-4 py-2 text-sm text-white font-medium bg-red-500 hover:bg-red-600 border border-red-600 rounded-md shadow-button">X</button>
            </form>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">@sortablelink('name', 'Titre')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('artist', 'Artiste')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('genre.name', 'Genre')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('milliseconds', 'Durée')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('unit_price', 'Prix')</th>
                <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
            </tr>
            </thead>
            <tbody>
            @foreach($tracks as $track)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{$track->name}}</th>
                    <td class="px-6 py-4">{{$track->album?->artist?->name ?? 'Artiste supprimé'}}</td>
                    <td class="px-6 py-4">{{$track->genre->name}}</td>
                    <td class="px-6 py-4">{{$track->minutes}}</td>
                    <td class="px-6 py-4">{{$track->unit_price}}Є</td>
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            @can('cart')
                                <form action="{{route('cart.add', $track)}}" method="POST">
                                    @csrf <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🛒</button>
                                </form>
                            @endcan
                            @can('playlists.delete')
                                <form action="{{route('playlists.destroy.track', ['playlist' => $playlist, 'track' => $track])}}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="ml-1 border hover:bg-gray-50 font-semibold py-1 px-2 rounded">❌</button>
                                </form>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($tracks->hasPages())
        <div class="mt-10">
            {{$tracks->links()}}
        </div>
    @endif
</x-layout>


