<?php

namespace App\Http\Controllers;

use App\Exports\ArtistsExport;
use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ArtistController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:artists.edit')->only(['edit', 'update']);
        $this->middleware('permission:artists.delete')->only(['destroy', 'restore']);
        $this->middleware('permission:artists.create')->only(['create', 'store']);
    }

    public function index(){
        $artists = auth()->user()->can('artists.delete') ? Artist::withTrashed() : Artist::withoutTrashed();
        $artists = $artists->sortable()
            ->withCount(['tracks'])
            ->orderBy('updated_at', 'desc')
            ->filter(\request()->input());
        return view('artists.index')->with('artists', $artists->paginate(15));
    }

    public function destroy(Artist $artist){
        $artist->delete();
        return back()->with('success', 'Suppression de l\'artiste avec succès !');
    }

    public function restore($artistId){
        $artist = Artist::onlyTrashed()->findOrFail($artistId);
        $artist->restore();
        return back()->with('success', 'Artiste restauré avec succès.');
    }

    public function edit(Artist $artist){
        return view('artists.edit')->with('artist', $artist);
    }

    public function create(){
        return view('artists.create')->with('artist', new Artist());
    }

    public function update(Request $request, Artist $artist){
        $formFields = [];

        if ($request->input('name') != $artist->name)
            $formFields = $request->validate(['name' => ['required', 'min:2', Rule::unique('artists', 'name')]]);

        if ($request->hasFile('pochette'))
            $formFields['pochette'] = $request->file('pochette')->store('artists', 'public');

        $artist->update($formFields);

        return back()->with('success', 'Modification réussie avec succès !');
    }

    public function store(Request $request){
        $formFields = $request->validate(['name' => ['required', 'min:2', Rule::unique('artists', 'name')]]);

        if ($request->hasFile('pochette'))
            $formFields['pochette'] = $request->file('pochette')->store('artists', 'public');

        Artist::create($formFields);

        return back()->with('success', 'Création réussie avec succès !');
    }

    public function export(){
        return Excel::download(new ArtistsExport, 'artists.xlsx');
    }
}
