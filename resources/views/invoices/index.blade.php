<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">commandes</span>
        </x-slot>
        @can('cart')
            <a href="{{route('cart.index')}}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter une commande
            </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">Id</th>
                <th scope="col" class="px-6 py-3">@sortablelink('invoice_date', 'Date')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('customer.lastname', 'Client')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('customer.email', 'Email')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('invoicesItems_count', 'Articles')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('total', 'Prix')</th>
                <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $invoice)
                <tr class="bg-white border-b">
                  <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{$invoice->id}}</th>
                    <td class="px-6 py-4">{{$invoice->invoice_date}}</td>
                    <td class="px-6 py-4">{{$invoice->customer->lastname}} {{$invoice->customer->firstname}}</td>
                    <td class="px-6 py-4">{{$invoice->customer->email}}</td>
                    <td class="px-6 py-4">{{$invoice->invoicesItems_count}}</td>
                    <td class="px-6 py-4">{{$invoice->total}}Є</td>
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            <a class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('invoices.show', $invoice)}}">🔎</a>
                            @can('invoices.delete')
                                <form action="{{route('invoices.destroy', $invoice)}}" method="POST">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">❌</button>
                                </form>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($invoices->hasPages())
        <div class="mt-10">
            {{$invoices->links()}}
        </div>
    @endif
</x-layout>
