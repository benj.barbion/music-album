<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TrackController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/* Index : show all * Show : show one * Create : show form to create * Store : store new * Edit : show form to edit * Update : update * Destroy : delete */
Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/register', [UserController::class, 'create'])->name('register');
Route::get('/login', [UserController::class, 'login'])->name('login');
Route::get('/logout', [UserController::class, 'logout'])->name('logout');

Route::name('users.')->group(function () {
    Route::post('/users/authenticate', [UserController::class, 'authenticate'])->name('authenticate');
    Route::post('/users', [UserController::class, 'store'])->name('store');
    Route::get('/account', [UserController::class, 'accountIndex'])->name('account');
    Route::patch('/users/{user}', [UserController::class, 'update'])->name('update');
});

Route::name('albums.')->group(function () {
    Route::get('/albums', [AlbumController::class, 'index'])->name('index');
    Route::get('/albums/create', [AlbumController::class, 'create'])->name('create');
    Route::post('/albums', [AlbumController::class, 'store'])->name('store');
    Route::get('/albums/export/', [AlbumController::class, 'export'])->name('export');
    Route::post('/albums/{album}/cart', [AlbumController::class, 'addToCart'])->name('cart');
    Route::get('/albums/{album}/export', [AlbumController::class, 'exportOne'])->name('export.one');
    Route::get('/albums/{album}/edit', [AlbumController::class, 'edit'])->name('edit');
    Route::post('/albums/{album}/restore', [AlbumController::class, 'restore'])->name('restore');
    Route::delete('/albums/{album}', [AlbumController::class, 'destroy'])->name('destroy');
    Route::put('/albums/{album}', [AlbumController::class, 'update'])->name('update');
    Route::get('/albums/{album}', [AlbumController::class, 'show'])->name('show');
});

Route::name('tracks.')->group(function () {
    Route::get('/tracks', [TrackController::class, 'index'])->name('index');
    Route::get('/tracks/create', [TrackController::class, 'create'])->name('create');
    Route::get('/tracks/{track}', [TrackController::class, 'edit'])->name('edit');
    Route::put('/tracks/{track}', [TrackController::class, 'update'])->name('update');
    Route::post('/tracks', [TrackController::class, 'store'])->name('store');
    Route::delete('/tracks/{track}', [TrackController::class, 'destroy'])->name('destroy');
    Route::post('/tracks/{track}/restore', [TrackController::class, 'restore'])->name('restore');
});

Route::name('artists.')->group(function () {
    Route::get('/artists', [ArtistController::class, 'index'])->name('index');
    Route::get('/artists/export', [ArtistController::class, 'export'])->name('export');
    Route::get('/artists/create', [ArtistController::class, 'create'])->name('create');
    Route::delete('/artists/{artist}', [ArtistController::class, 'destroy'])->name('destroy');
    Route::get('/artists/{artist}/edit', [ArtistController::class, 'edit'])->name('edit');
    Route::post('/artists/{artist}/restore', [ArtistController::class, 'restore'])->name('restore');
    Route::put('/artists/{artist}', [ArtistController::class, 'update'])->name('update');
    Route::post('/artists', [ArtistController::class, 'store'])->name('store');
});

Route::name('playlists.')->group(function () {
    Route::get('/playlists', [PlayListController::class, 'index'])->name('index');
    Route::get('/playlists/{playlist}/export', [PlaylistController::class, 'export'])->name('export');
    Route::get('/playlists/create', [PlaylistController::class, 'create'])->name('create');
    Route::get('/playlists/{playlist}', [PlayListController::class, 'show'])->name('show');
    Route::get('/playlists/{playlist}/create', [PlaylistController::class, 'createTrack'])->name('tracks.create');
    Route::post('/playlists/{playlist}', [PlayListController::class, 'storeTrack'])->name('tracks.store');
    Route::delete('/playlists/{playlist}', [PlaylistController::class, 'destroy'])->name('destroy');
    Route::delete('/playlists/{playlist}/{track}', [PlayListController::class, 'destroyTrack'])->name('destroy.track');
    Route::post('/playlists', [PlayListController::class, 'store'])->name('store');
});

Route::name('invoices.')->group(function () {
    Route::get('/invoices', [InvoiceController::class, 'index'])->name('index');
    Route::delete('/invoices/{invoice}', [InvoiceController::class, 'destroy'])->name('destroy');
    Route::get('/invoices/{invoice}', [InvoiceController::class, 'show'])->name('show');
});

Route::name('cart.')->group(function () {
    Route::get('/cart', [CartController::class, 'index'])->name('index');
    Route::get('/cart/success', [CartController::class, 'success'])->name('success');
    Route::post('/cart', [CartController::class, 'store'])->name('store');
    Route::post('/cart/create-step-one', [CartController::class, 'createStepOneStore'])->name('create.step.one.store');
    Route::post('/cart/{track}', [CartController::class, 'addToCart'])->name('add');
    Route::delete('/cart/{id}', [CartController::class, 'deleteFromCart'])->name('delete');
    // we could use PUT if route --> /cart/{id}/quantity
    Route::patch('/cart/{id}', [CartController::class, 'update'])->name('update');
});

Route::name('customers.')->group(function () {
    Route::get('/customers', [CustomerController::class, 'index'])->name('index');
    Route::get('/customers/create', [CustomerController::class, 'create'])->name('create');
    Route::delete('/customers/{customer}', [CustomerController::class, 'destroy'])->name('destroy');
    Route::post('/customers', [CustomerController::class, 'store'])->name('store');
    Route::get('/customers/{customer}/edit', [CustomerController::class, 'edit'])->name('edit');
    Route::post('/customers/{customer}/restore', [CustomerController::class, 'restore'])->name('restore');
    Route::put('/customers/{customer}', [CustomerController::class, 'update'])->name('update');
});

Route::name('employees.')->group(function () {
    Route::get('/employees', [EmployeeController::class, 'index'])->name('index');
    Route::get('/employees/create', [EmployeeController::class, 'create'])->name('create');
    Route::delete('/employees/{employee}', [EmployeeController::class, 'destroy'])->name('destroy');
    Route::post('/employees', [EmployeeController::class, 'store'])->name('store');
    Route::get('/employees/{employee}/edit', [EmployeeController::class, 'edit'])->name('edit');
    Route::post('/employees/{employee}/restore', [EmployeeController::class, 'restore'])->name('restore');
    Route::put('/employees/{employee}', [EmployeeController::class, 'update'])->name('update');
    Route::post('/employees/{employee}/account', [EmployeeController::class, 'storeAccount'])->name('store.account');
});

Route::name('roles.')->group(function () {
    Route::get('/roles', [RoleController::class, 'index'])->name('index');
    Route::get('/roles/create', [RoleController::class, 'create'])->name('create');
    Route::post('/roles', [RoleController::class, 'store'])->name('store');
    Route::get('/roles/{role}/edit', [RoleController::class, 'edit'])->name('edit');
    Route::put('/roles/{role}', [RoleController::class, 'update'])->name('update');
    Route::delete('/roles/{role}', [RoleController::class, 'destroy'])->name('destroy');
});

Route::fallback(fn () => abort(404));
