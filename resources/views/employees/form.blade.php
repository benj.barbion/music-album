<div class="flex -mx-3 mt-4">
    <div class="flex-col w-1/2 px-3">
        <label for="lastname">Nom:</label>
        <div class="w-full bg-blueGray-50 rounded">
            <input value="{{$employee?->lastname ?? @old('lastname')}}"
                   class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('lastname') border-2 border-red-500 @enderror"
                   type="text" name="lastname" required>
            @error('lastname')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror
        </div>
    </div>
    <div class="flex-col w-1/2 px-3">
        <label for="firstname">Prénom:</label>
        <div class="flex w-full bg-blueGray-50 rounded">
            <input value="{{$employee?->firstname ?? @old('firstname')}}"
                   class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('firstname') border-2 border-red-500 @enderror"
                   type="text" name="firstname" required>
            @error('firstname')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror
        </div>
    </div>
</div>

<label for="country" class="block mt-4">Titre:</label>
<input value="{{$employee->title ?? @old('title')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none"
       type="text" name="title">

<label for="address" class="block mt-4">Adresse:</label>
<input value="{{$employee->address ?? @old('address')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('address') border-2 border-red-500 @enderror"
       type="text" name="address">

<div class="flex -mx-3 mt-4">
    <div class="flex-col w-1/3 px-3">
        <label for="postalcode">Code postal:</label>
        <div class="flex w-full bg-blueGray-50 rounded">
            <input value="{{$employee->postalcode ?? @old('postalcode')}}"
                   class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('postalcode') border-2 border-red-500 @enderror"
                   type="text" name="postalcode">
        </div>
    </div>
    <div class="flex-col w-2/3 px-3">
        <label for="city">Ville:</label>
        <div class="flex w-full bg-blueGray-50 rounded">
            <input value="{{$employee->city ?? @old('city')}}"
                   class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none"
                   type="text" name="city">
        </div>
    </div>
</div>

<label for="country" class="block mt-4">Pays:</label>
<input value="{{$employee->country ?? @old('country')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none"
       type="text" name="country">

<label for="phone" class="block mt-4">Téléphone:</label>
<input value="{{$employee->phone ?? @old('phone')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none"
       type="text" name="phone">

<label for="company" class="block mt-4">Email:</label>
<input value="{{$employee->email ?? @old('email')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('email') border-2 border-red-500 @enderror"
       type="email" name="email" required>
@error('email')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror
