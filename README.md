<img src="https://zupimages.net/up/23/22/vz9m.png" alt="Music APP" width="70%">

## Informations générales (et essentielles)

#### Etapes à suivre lors de la récupération du projet :

1. Utilisez **exactement la même BDD** donnée lors de l'énoncé du projet
2. Installez les dépendances via la commande **composer install**
3. **Modifier les accès** BDD dans le fichier sensible .env 
4. Utilisez la commande artisan afin d'effectuer la migration de la BDD :
**php artisan migrate:refresh --seed**
5. Lancez le projet via la commande **php artisan serve**
6. **Take a beer and enjoy !**

####  Ce que la migration effectue dans la BDD :

- **Ajout de plusieurs règles essentielles au programme :**
  1. Création de la table Users.
  2. Ajout de la gestion des pochettes pour les albums.
  3. Ajout de la gestion des pochettes pour les utilisateurs.
  4. Ajout des tables concernant les permissions.
  5. Ajout d'un lien entre la table employees et users.
  6. Ajout des conditions de soft deletes.


- **Ajout de plusieurs données :**
  1. Ajout de plusieurs rôles essentiels.
  2. Ajout de plusieurs utilisateurs (dont certains liés aux employés).
  3. Ajout de plusieurs albums contenant une pochette.
  4. Ajout et modification d'artistes contenant une pochette.


## Les comptes utilisateurs (à travers leurs rôles)

Votre application est fournie avec une liste d'utilisateurs, possédant certains rôles (qui eux, contiennent des permissions).

### 1. Le héro, le super-admin !
 **Email :** b.barbion@gmail.com<br />**Mot de passe :** Benjamin<br />
**Pouvoirs :** TOUT

### 2. Le général manager, le super-admin de wish
**Email :** andrew@chinookcorp.com<br />**Mot de passe :** Adams<br />

###### Liste des pouvoirs spéciaux : 
1. employees.* 
2. albums.*
3. artists.*
4. cart
5. invoices.*
6. playlists.*
7. tracks.*
8. customers.*

### 3. Le sale manager, le général manager pas assez doué
**Email :** nancy@chinookcorp.com<br />**Mot de passe :** Edwards<br />

###### Liste des pouvoirs spéciaux :
1. customers.*
2. albums.*
3. cart
4. invoices.show
5. tracks.create

### 4. L'agent, celui qu'on aime pas
**Email :** jane@chinookcorp.com<br />**Mot de passe :** Peacock

**Email :** margaret@chinookcorp.com<br />**Mot de passe :** Park

###### Liste des pouvoirs wish :
1. cart
2. invoices.show
3. customers.show

### 5. Les autres utilisateurs (normaux)

**Email :** r.rene@gmail.com<br />**Mot de passe :** René<br />
**Pouvoirs :** lire, lire et... lire

Un visiteur peut s'inscrire sur le site s'il le souhaite, il n'aura accès qu'en lecture à certaines parties du site.

### 6. Bonus

Vous pouvez ajouter des employés à travers le site, et leur créer un compte utilisateur s'ils en ont le besoin ! Vous pourrez ensuite modifier leurs rôles à travers l'édition des employés.

## Gestion du panier

Un employé possédant la **permission cart** pourra remplir le panier et **effectuer une commande** pour l'un des clients. Il pourrait également être envisagé, dans le futur, qu'un simple client puisse lui-même effectuer la commande.

**Liste des étapes :**
1. Choisissez le client
2. Choisissez vos articles (que ce soit via les pages playlists, albums ou titres)
3. Validez le panier (vous y verrez le total)

Par ailleurs, il est également possible de remplir le panier sans pour autant encore avoir sélectionné un client. Vous devrez évidemment en choisir un avant de pouvoir valider définitivement le panier.

## Indice de popularité

L'application fourni une configuration pour l'indice de popularité des chansons par défaut. 

Ce fichier est évidemment modifiable via **config/popularity.php**

**Voici la configuration par défaut :** 

``` php
return [
    ['to' => 5, 'result' => 'Aucune'],
    ['to' => 40, 'result' => '❤'],
    ['to' => 100, 'result' => '❤❤'],
    ['to' => 1000, 'result' => '❤❤❤'],
    ['to' => 10000, 'result' => '❤❤❤❤']
];
```

Le to signifie "jusqu'à combien de ventes", le result est ce qui s'affiche si la condition est remplie.

Autrement dit, par défaut, un titre affichera "aucune" s'il est vendu moins de 5 fois, etc.

![Semantic description of image](https://www.zupimages.net/up/22/24/t8jd.png "Popularity")

## Packages utilisés
- [Spatie, pour des autorisations simples et efficaces](https://github.com/spatie/laravel-permission)
- [Laravel-Excel, pour un export efficace des albums](https://github.com/SpartnerNL/Laravel-Excel)
- [LaravelShoppingcart, pour une utilisation plus aisée d'un panier](https://github.com/hardevine/LaravelShoppingcart)
- [Column-Sortable, pour pouvoir trier nos colonnes](https://github.com/Kyslik/column-sortable)
- [Laravel-Breadcrumbs, car c'est trop joli d'avoir l'arborescence](https://github.com/d13r/laravel-breadcrumbs)

## Pour approfondir et améliorer le projet...

https://laravel.com/docs/9.x/

Notamment pour utiliser **Route::prefix**, **Route::resource** ou encore comprendre à bon escient **Eloquent et Query Builder**, chacun ayant leurs avantages.
