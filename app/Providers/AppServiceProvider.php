<?php

namespace App\Providers;

use App\Models\Album;
use App\Models\Artist;
use App\Models\Playlist;
use App\Models\Track;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use stdClass;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasColumn('albums', 'deleted_at')) {
            $object = new stdClass();
            $object->userCount = Album::count('id');
            $object->playlistCount = Playlist::count('id');
            $object->artistCount = Artist::count('id');
            $object->trackCount = Track::count('id');
            \View::share('stats', $object);
        }
    }
}
