<x-layout>
    {{ Breadcrumbs::render() }}

    <x-search title="des employés" searchBy="Cherchez par nom, prénom, ville, email ou encore titre." />

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">employés</span>
        </x-slot>
        @can('employees.create')
            <a href="{{ route('employees.create') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter un employé
            </a>
        @endcan
    </x-title>

    <div class="mt-5 bg-blue-100 border-l-4 border-blue-500 text-blue-700 p-4" role="alert">
        <p>Besoin de créer un compte utilisateur à l'un de vos employés ? <strong>Créez-le directement via la page édition !</strong><br /> Vous pourrez ensuite directement lui assigner des rôles via cette même page.</p>
    </div>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-5">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">@sortablelink('lastname', 'Nom')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('email', 'Email')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('phone', 'Téléphone')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('city', 'Ville')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('country', 'Pays')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('title', 'Titre')</th>
                <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr class="bg-white border-b">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{ $employee->lastname }} {{ $employee->firstname }}</th>
                    <td class="px-6 py-4">{{ $employee->email }}</td>
                    <td class="px-6 py-4">{{ $employee->phone ?? '-' }}</td>
                    <td class="px-6 py-4">{{ $employee->city ?? '-' }}</td>
                    <td class="px-6 py-4">{{ $employee->country ?? '-' }}</td>
                    <td class="px-6 py-4">{{ $employee->title ?? '-' }}</td>
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            @unless($employee->trashed())
                                @can('employees.edit')
                                    <a class="border rounded hover:bg-gray-50 font-semibold py-1 px-2" href="{{route('employees.edit', $employee)}}">✏</a>
                                @endcan
                                @can('employees.delete')
                                    <form action="{{route('employees.destroy', $employee)}}" method="POST">
                                        @csrf @method('DELETE') <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-2">❌</button>
                                    </form>
                                @endcan
                            @else
                                <form action="{{ route('employees.restore', $employee) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                                </form>
                            @endunless
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($employees->hasPages())
        <div class="mt-10">
            {!! $employees->appends(\Request::except('page'))->render() !!}
        </div>
    @endif
</x-layout>
