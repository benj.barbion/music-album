<x-layout>
    {{ Breadcrumbs::render() }}

    <x-search title="des titres" searchBy="Cherchez par nom du titre, son genre, ou encore l'artiste." />

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">titres</span>
        </x-slot>
        @can('tracks.create')
            <a href="{{ route('tracks.create') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter un titre
            </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">@sortablelink('name', 'Titre de la piste')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('artist', 'Artiste')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('genre.name', 'Genre')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('milliseconds', 'Durée')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('unit_price', 'Prix')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('invoice_items_sum_quantity', 'Popularité')</th>
                @canany(['cart', 'tracks.edit', 'tracks.delete'])
                    <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($tracks as $track)
                <tr class="bg-white border-b">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                        <p class="font-semibold">{{$track->name}}</p>
                        <p class="text-blueGray-400">{{$track->album?->title}}</p>
                    </th>
                    <td class="px-6 py-4">{{$track->artist}}</td>
                    <td class="px-6 py-4">{{$track->genre->name}}</td>
                    <td class="px-6 py-4">{{$track->minutes}}</td>
                    <td class="px-6 py-4">{{$track->unit_price}}Є</td>
                    <td class="px-6 py-4">{{$track->popularity}}</td>
                    @canany(['cart', 'tracks.edit', 'tracks.delete'])
                        <td class="px-6 py-4">
                            <div class="flex justify-end">
                                @unless($track->trashed())
                                    @can('cart')
                                        <form action="{{route('cart.add', $track)}}" method="POST">
                                            @csrf <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🛒</button>
                                        </form>
                                    @endcan
                                    @can('tracks.edit')
                                        <a class="border hover:bg-gray-50 font-semibold py-1 px-2 ml-1 rounded" href="{{route('tracks.edit', $track)}}">✏</a>
                                    @endcan
                                    @can('tracks.delete')
                                        <form action="{{route('tracks.destroy', $track)}}" method="POST">
                                            @csrf @method('DELETE')
                                            <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">❌</button>
                                        </form>
                                    @endcan
                                @else
                                    <form action="{{ route('tracks.restore', $track) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                                    </form>
                                @endunless
                            </div>
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($tracks->hasPages())
        <div class="mt-10">
            {!! $tracks->appends(\Request::except('page'))->render() !!}
        </div>
    @endif
</x-layout>
