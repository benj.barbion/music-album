<?php

use App\Models\Album;
use App\Models\Artist;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Playlist;
use App\Models\Track;
use App\Models\User;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;
use Spatie\Permission\Models\Role;

Breadcrumbs::for('albums.index', function (BreadcrumbTrail $trail): void {
    if (request()->has('artist'))
        $trail->parent('artists.index');
    $trail->push('Albums', route('albums.index'));
});

Breadcrumbs::for('albums.show', function (BreadcrumbTrail $trail, Album $album): void {
    $trail->parent('albums.index');
    $trail->push($album->title, route('albums.show', $album));
});

Breadcrumbs::for('albums.edit', function (BreadcrumbTrail $trail, Album $album): void {
    $trail->parent('albums.show', $album);
    $trail->push('Editer', route('albums.show', $album));
});

Breadcrumbs::for('albums.create', function (BreadcrumbTrail $trail): void {
    $trail->parent('albums.index');
    $trail->push('Créer un album');
});

Breadcrumbs::for('users.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Users', route('users.index'));
});

Breadcrumbs::for('users.show', function (BreadcrumbTrail $trail, User $user): void {
    $trail->parent('users.index');
    $trail->push($user->name, route('users.show', $user));
});

Breadcrumbs::for('users.edit', function (BreadcrumbTrail $trail, User $user): void {
    $trail->parent('users.show');
    $trail->push('Edit', route('users.edit', $user));
});

Breadcrumbs::for('artists.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Artistes', route('artists.index'));
});

Breadcrumbs::for('artists.show', function (BreadcrumbTrail $trail, Artist $model): void {
    $trail->parent('artists.index');
    $trail->push($model->name);
});

Breadcrumbs::for('artists.edit', function (BreadcrumbTrail $trail, Artist $model): void {
    $trail->parent('artists.index');
    $trail->push('Editer un artiste');
});

Breadcrumbs::for('artists.create', function (BreadcrumbTrail $trail): void {
    $trail->parent('artists.index');
    $trail->push('Créer un artiste');
});

Breadcrumbs::for('playlists.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Playlists', route('playlists.index'));
});

Breadcrumbs::for('playlists.create', function (BreadcrumbTrail $trail): void {
    $trail->parent('playlists.index');
    $trail->push('Créer une playlist');
});

Breadcrumbs::for('playlists.show', function (BreadcrumbTrail $trail, Playlist $playlist): void {
    $trail->parent('playlists.index');
    $trail->push($playlist->name, route('playlists.show', $playlist));
});

Breadcrumbs::for('playlists.tracks.create', function (BreadcrumbTrail $trail, Playlist $playlist): void {
    $trail->parent('playlists.show', $playlist);
    $trail->push('Ajouter un titre');
});

Breadcrumbs::for('tracks.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Titres', route('tracks.index'));
});

Breadcrumbs::for('tracks.edit', function (BreadcrumbTrail $trail, Track $track): void {
    $trail->parent('tracks.index');
    $trail->push('Editer '.$track->name, route('tracks.edit', $track));
});

Breadcrumbs::for('tracks.create', function (BreadcrumbTrail $trail): void {
    if (request()->has('album'))
    {
        $album = Album::find(request()->input('album'));
        if ($album != null)
            $trail->parent('albums.show', $album);
    }
    else
        $trail->parent('tracks.index');

    $trail->push('Créer un titre', route('tracks.create'));
});

Breadcrumbs::for('invoices.index', function (BreadcrumbTrail $trail): void {
    if (request()->has('customer'))
        $trail->push('Liste des clients', route('customers.index'));
    $trail->push('Commandes', route('invoices.index'));
});

Breadcrumbs::for('invoices.show', function (BreadcrumbTrail $trail, Invoice $invoice): void {
    $trail->parent('invoices.index');
    $trail->push('Détail commande', route('invoices.show', $invoice));
});

Breadcrumbs::for('customers.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Clients', route('customers.index'));
});

Breadcrumbs::for('customers.edit', function (BreadcrumbTrail $trail, Customer $customer): void {
    $trail->parent('customers.index');
    $trail->push('Editer '.$customer->firstname, route('customers.edit', $customer));
});

Breadcrumbs::for('employees.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Liste des employés', route('employees.index'));
});

Breadcrumbs::for('employees.create', function (BreadcrumbTrail $trail): void {
    $trail->parent('employees.index');
    $trail->push('Créer un employé', route('employees.create'));
});

Breadcrumbs::for('employees.edit', function (BreadcrumbTrail $trail, \App\Models\Employee $employee): void {
    $trail->parent('employees.index');
    $trail->push('Editer '.$employee->firstname, route('employees.edit', $employee));
});

Breadcrumbs::for('roles.index', fn(BreadcrumbTrail $trail) => $trail->push('Liste des rôles', route('roles.index')));

Breadcrumbs::for('roles.edit', function (BreadcrumbTrail $trail, Role $role): void {
    $trail->parent('roles.index');
    $trail->push('Editer '.$role->name, route('roles.edit', $role));
});

Breadcrumbs::for('roles.create', function (BreadcrumbTrail $trail): void {
    $trail->parent('roles.index');
    $trail->push('Créer un rôle', route('roles.index'));
});
