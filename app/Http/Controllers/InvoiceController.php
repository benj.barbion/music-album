<?php

namespace App\Http\Controllers;

use App\Models\Invoice;

class InvoiceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:invoices.show')->only('index', 'show');
        $this->middleware('permission:invoices.delete')->only('destroy');
    }

    public function index(){
        return view('invoices.index')
            ->with('invoices', Invoice::sortable()->orderBy('invoice_date', 'desc')->filter(\request(['customer']))->paginate(15));
    }

    public function show(Invoice $invoice){
        $invoice->load('invoicesItems.track.album.artist:id,name'); // eagger loading on existing model
        return view('invoices.show')->with('invoice', $invoice);
    }

    public function destroy(Invoice $invoice){
        $invoice->delete();
        return back()->with('success', 'Suppression avec succès.');
    }
}
