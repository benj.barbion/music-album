<x-layout>
    <x-stepsbar id="1" />
    <div class="container px-4 mx-auto">
        <div class="flex max-w-lg mx-auto flex-col">
            <div class="p-8 bg-white rounded shadow">
                <div class=" text-center">
                    <span class="text-sm text-blueGray-400 content-center">Création commande</span>
                    <h4 class="mb-6 text-3xl">Choisissez un client</h4>
                </div>
                <form method="POST" action="{{ route('cart.create.step.one.store') }}" role="form" enctype="multipart/form-data">
                    @csrf
                    <select class="mt-4 appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none" name="customer_id" required>
                        <option value="0">--Sélectionner le client--</option>
                        @foreach($customers as $customer)
                            <option value="{{$customer->id}}">{{$customer->fullName()}}</option>
                        @endforeach
                    </select>
                    @error('artist')<p class="text-red-500 text-xs">{{$message}}</p>@enderror
                    <button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>
