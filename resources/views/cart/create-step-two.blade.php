<x-layout>
    <x-stepsbar id="2" />
    <div class="container px-4 mx-auto">
        <x-title>
            <x-slot name="pageHeading">
                Choisissez les <span class="text-blue-600">titres</span> dans le catalogue
            </x-slot>
            <a href="{{route('cart.index', ['cancel' => 1])}}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-red-500 hover:bg-red-600 border border-red-600 rounded-md shadow-button">
                Annuler
            </a>
        </x-title>
        <div class="flex flex-wrap -mx-4 mb-14 mt-10">
            <div class="w-full md:w-8/12 xl:w-9/12 px-4 mb-14 md:mb-0">
                <div class="py-10 px-8 md:px-12 bg-white rounded-3xl">
                    <span class="inline-block text-darkBlueGray-300 font-medium">{{Cart::count()}} {{Str::plural('article', Cart::count())}} dans votre panier</span>
                    @foreach(Cart::content() as $elem)
                        <div class="relative flex flex-wrap items-center xl:justify-between -mx-4 mt-8 pt-8 border-t border-gray-200 border-opacity-40 transform odd:rotate-45">
                            <div class="w-full md:w-auto px-4 mb-6 xl:mb-0">
                                <a class="block mb-5 text-xl font-heading font-medium" href="#">{{$elem->name}}</a>
                                <div class="flex flex-wrap">
                                    <p class="mr-4 text-sm font-medium">
                                        <span class="font-heading">Artiste:</span>
                                        <span class="ml-2 text-gray-400">{{$elem->options->artiste}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="flex items-center left-auto xl:w-auto px-4 mb-6 xl:mb-0 mt-6 xl:mt-0">
                                <span class="text-xl font-heading font-medium text-blue-500"><span>{{$elem->price}} Є</span></span>
                                <h4 class="mx-4 ml-6 font-heading font-medium">Qty:</h4>
                                <form action="{{route('cart.update', $elem->rowId)}}" method="post">
                                    @method('PATCH')
                                    @csrf
                                    <input name="id" value="{{$elem->idRow}}" type="hidden">
                                    <input name="quantity" value="{{$elem->qty}}" class="w-16 px-3 py-2 text-center placeholder-gray-400 text-gray-400 bg-blue-50 border-2 border-blue-400 outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50 rounded-xl number-no-style" type="number" min="1">
                                </form>
                                <form action="{{ route('cart.delete', $elem->rowId) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="mt-2 ml-4 text-gray-200 hover:text-gray-300 focus:outline-none">
                                        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="0.5" y="0.5" width="27" height="27" rx="13.5" stroke="currentColor"></rect><line x1="20.495" y1="8.49497" x2="8.49498" y2="20.495" stroke="currentColor" stroke-width="1.4"></line><line x1="19.505" y1="20.495" x2="7.50503" y2="8.49498" stroke="currentColor" stroke-width="1.4"></line></svg>
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="w-full md:w-4/12 xl:w-3/12 px-4">
                <div class="mb-14">
                    <h2 class="mb-7 text-3xl font-heading font-medium">Détails</h2>
                    <div class="flex items-center justify-between py-4 px-10 mb-3 leading-8 bg-white bg-opacity-50 font-heading font-medium rounded-3xl mt-2">
                        <span>HTVA</span>
                        <span class="flex items-center text-xl">{{Cart::subtotal()}} Є</span>
                    </div>
                    <div class="flex items-center justify-between py-4 px-10 mb-3 leading-8 bg-white bg-opacity-50 font-heading font-medium rounded-3xl">
                        <span>TVA</span>
                        <span class="flex items-center text-xl">{{Cart::tax()}} Є</span>
                    </div>
                    <div class="flex items-center justify-between py-4 px-10 mb-3 leading-8 bg-white font-heading font-medium rounded-3xl">
                        <span>TOTAL</span>
                        <span class="flex items-center text-xl text-blue-500">{{Cart::total()}} Є</span>
                    </div>
                    <form action="{{ route('cart.store') }}" method="POST">
                        @csrf
                        <button type="submit" class="inline-block text-center cursor-pointer w-full px-4 py-3 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button @if(Cart::count() < 1) opacity-50 cursor-not-allowed @endif" @if(Cart::count() < 1) disabled @endif>
                            Valider la commande
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-layout>

