<x-layout>
    <x-stepsbar id="3" />
    <div class="container px-4 mx-auto">
        <div class="flex max-w-lg mx-auto flex-col">
            <div class="p-8 bg-white rounded shadow">
                <div class=" text-center">
                    <span class="text-sm text-blueGray-400 content-center">Création commande</span>
                    <h4 class="mb-6 text-3xl">Finalisation</h4>
                </div>
                <h2 class="text-1xl md:text-2xl font-bold font-heading">Waooooooouww ! Commande validée avec succès.</h2>
                <div class="flex justify-between mt-10">
                    <a href="{{route('index')}}" class="cursor-pointer p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">
                        Retour à l'accueil
                    </a>
                    <a href="{{route('cart.index')}}" class="cursor-pointer p-4 text-center text-xs text-white font-semibold leading-none bg-green-500 hover:bg-green-600 rounded">
                        Ajouter une autre commande
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-layout>
