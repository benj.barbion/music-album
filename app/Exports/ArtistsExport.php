<?php

namespace App\Exports;

use App\Models\Artist;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ArtistsExport implements FromView, ShouldAutoSize, WithStrictNullComparison
{
    public function view(): View
    {
        return view('exports.artists', [
            'artists' => Artist::withoutTrashed()->get()
        ]);
    }
}
