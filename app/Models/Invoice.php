<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kyslik\ColumnSortable\Sortable;

class Invoice extends Model
{
    use HasFactory, Sortable;

    protected $sortable = ['invoice_date', 'customer.lastname', 'customer.email', 'total'];
    protected $sortableAs = ['invoicesItems_count'];
    protected $fillable = ['customer_id', 'invoice_date', 'total'];
    protected $with = ['customer'];
    protected $withCount = ['invoicesItems as invoicesItems_count'];
    protected $dates = ['invoice_date'];

    public function customer(): BelongsTo {
        return $this->belongsTo(Customer::class)->withTrashed()->select('id', 'firstname', 'lastname', 'email');
    }

    public function invoicesItems(): HasMany {
        return $this->hasMany(InvoiceItem::class);
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['customer'] ?? false) {
            return $query->whereHas('customer', function ($query) {
                $query->where('id', '=', request('customer'));
            });
        }
    }

    protected static function boot() {
        parent::boot();
        static::deleting(fn($invoice) => $invoice->invoicesItems()->delete());
    }
}
