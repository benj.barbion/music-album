<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['accountIndex', 'update', 'logout']);
        $this->middleware('guest')->only(['login','create', 'store', 'authenticate']);
    }

    public function create(){
        return view('users.register', ['user' => new User]);
    }

    public function login(){
        return view('users.login', ['user' => new User]);
    }

    public function store(Request $request){
        $formFields = $request->validate([
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'min:3', Rule::unique('users')],
            'password' => 'required|confirmed|min:6'
        ]);

        $formFields['password'] = bcrypt($formFields['password']);

        $user = User::create($formFields);

        Auth::login($user);

        return redirect()->route('index')->with('success', 'Création réussie avec succès !');
    }

    public function authenticate(Request $request){
        $formFields = $request->validate([
            'email' => ['required', 'email'],
            'password' => 'required'
        ]);

        $formFields['deleted_at'] = null; // refuse soft deleted users

        if (Auth::attempt($formFields)){
            $request->session()->regenerate();
            return redirect()->route('index')->with('success', 'Connexion réussie !');
        }

        return back()->with('error', 'Informations de connexion invalides.');
    }

    public function accountIndex(){
       return view('users.account');
    }

    public function update(Request $request, User $user){
        if ($user->id != $request->user()->id)
            abort(403, 'Unauthorized action.');

        $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if(!Hash::check($request->input('old_password'), $user->password))
            return back()->withErrors(['old_password' => 'Votre ancien mot de passe est incorrect.']);

        $user->password = bcrypt($request->input('password'));
        $user->save();

        return back()->with('success', 'Mot de passe modifié avec succès.');
    }

    public function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('index')->with('success', 'Vous avez été déconnecté.');
    }
}
