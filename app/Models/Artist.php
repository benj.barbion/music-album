<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Artist extends Model
{
    use HasFactory, SoftDeletes, Sortable;

    protected $sortable = ['name', 'updated_at'];
    protected $sortableAs = ['tracks_count', 'albums_count'];
    protected $fillable = ['pochette', 'name'];
    protected $withCount = ['albums', 'tracks'];

    public function albums() {
        return $this->hasMany(Album::class, 'artist_id');
    }

    public function tracks() {
        return $this->hasManyThrough(Track::class, Album::class);
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['search'] ?? false)
            return $query->where('name', 'like', '%' . $filter['search'] . '%');

        if ($filter['artist'] ?? false)
            return $query->where('id', '=', $filter['artist']);

        return $query;
    }

    public static function boot() {
        parent::boot();
        static::deleting(fn($artist) => $artist->albums()->delete());
        static::restoring(fn($artist) => $artist->albums()->restore());
    }
}
