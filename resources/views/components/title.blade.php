<section>
    <div class="flex flex-wrap items-center justify-between pb-5 -mx-2">
        <div class="w-full md:w-1/2 px-2 mb-4 md:mb-0">
            <h3 class="text-3xl mb-2 leading-tight font-bold font-heading">
                {{$pageHeading}}
            </h3>
        </div>
        <div class="w-full md:w-1/2 px-2">
            <div class="flex flex-wrap justify-end -m-2">
                <div class="w-full md:w-auto p-2">
                    {{$slot}}
                </div>
            </div>
        </div>
    </div>
    <div class="border-b border-coolGray-100"></div>
</section>
