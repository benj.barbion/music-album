<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Album extends Model
{
    use HasFactory, SoftDeletes, Sortable;

    protected $sortable = ['title', 'updated_at', 'artist.name'];
    protected $sortableAs = ['tracks_count'];
    protected $fillable = ['title', 'artist_id', 'pochette'];
    protected $with = ['artist'];
    protected $withCount = ['tracks as tracks_count'];

    public function artist(): BelongsTo {
        return $this->belongsTo(Artist::class, 'artist_id')->withTrashed();
    }

    public function tracks(): HasMany {
        return $this->hasMany(Track::class, 'album_id');
    }

    public function invoices() {
        return $this->hasManyThrough(InvoiceItem::class, Track::class);
    }

    public function scopePopular($query, int $total = 5) {
        return $query->withSum('invoices as total_invoices_album', 'quantity')
                     ->orderBy('total_invoices_album', 'desc')
                     ->take($total);
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['search'] ?? false) {
            return $query
                ->where('title', 'like', '%' . request('search') . '%')
                ->orWhereHas('artist', function ($artist) {
                    $artist->where('name', 'like', '%' . request('search') . '%');
                });
        }

        if ($filter['artist'] ?? false)
            return $query->where('artist_id', '=', request('artist'));

        return $query;
    }

    public static function orderedAlbums() {
        return self::orderBy('title', 'asc')->get(['id', 'title']);
    }

    public static function boot() {
        parent::boot();
        static::deleting(fn($album) => $album->tracks()->delete());
        static::restoring(fn($album) => $album->tracks()->restore());
    }
}
