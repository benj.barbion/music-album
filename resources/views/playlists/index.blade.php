<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">playlists</span>
        </x-slot>
        @can('playlists.create')
            <a href="{{route('playlists.create')}}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter une playlist
            </a>
        @endcan
    </x-title>

    <div class="flex flex-wrap mt-10 -mb-4">
        @foreach($playlists as $playlist)
            <div class="w-full md:w-1/2 lg:w-1/3 px-3 mb-4">
                <a href="{{route('playlists.show', $playlist->id)}}">
                    <div class="py-2 px-2 text-center bg-white rounded shadow">
                        <h3 class="mb-2 text-2xl font-bold font-heading">{{$playlist->name}}</h3>
                        <span class="text-3xl text-blue-600 font-bold font-heading">{{$playlist->tracks_count}}</span>
                        <p class="mt-2 text-blueGray-400">titres présents</p>
                    </div>
                </a>
            </div>
        @endforeach
    </div>

    @if ($playlists->hasPages())
        <div class="mt-10">
            {{$playlists->links()}}
        </div>
    @endif
</x-layout>
