<?php

namespace App\Http\Controllers;

use App\Exports\PlaylistExport;
use App\Models\Playlist;
use App\Models\Track;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class PlaylistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:playlists.create')->only(['create', 'createTrack', 'store', 'storeTrack']);
        $this->middleware('permission:playlists.delete')->only(['destroy', 'destroyTrack']);
    }

    public function export(Playlist $playlist){
        return Excel::download(new PlaylistExport($playlist), 'playlist_'.$playlist->name.'.xlsx');
    }

    public function index(){
        $playlists = Playlist::withCount('tracks')->latest()->paginate(15);
        return view('playlists.index')->with('playlists', $playlists);
    }

    public function show(Playlist $playlist){
        $tracks = $playlist->tracks()->sortable()->with('album.artist:id,name')->paginate(15);

        return view('playlists.show')
            ->with('playlist', $playlist)
            ->with('tracks', $tracks);
    }

    public function create(){
        return view('playlists.create');
    }

    public function createTrack(Playlist $playlist){
        // taking all id's that already in that playlist
        $existingIds = $playlist->tracks()->pluck('id');
        $tracks = Track::whereNotIn('id', $existingIds)->orderBy('name')->get(['id', 'name']);
        return view('playlists.tracks.create')
            ->with('playlist', $playlist)
            ->with('tracks', $tracks);
    }

    public function store(Request $request){
        $formFields = $request->validate([
            'name' => ['required', 'min:3', Rule::unique('playlists')]
        ]);
        Playlist::create($formFields);
        return back()->with('success', 'Création réussie avec succès !');
    }

    public function storeTrack(Request $request, Playlist $playlist) {
        $formFields = $request->validate([
            'track_id' => ['required', Rule::unique('playlist_track')->where(fn ($query) => $query->where('playlist_id', $playlist->id))]
        ]);
        $playlist->playlistTracks()->create($formFields);
        return back()->with('success', 'Création réussie avec succès !');
    }

    public function destroy(Playlist $playlist){
        $playlist->deleteOrFail();
        return redirect()->route('playlists.index')->with('success', 'Suppression avec succès !');
    }

    public function destroyTrack(Playlist $playlist, Track $track){
        $playlist->playlistTracks()->where('track_id', '=', $track->id)->delete();
        return back()->with('success', 'Suppression avec succès !');
    }
}
