<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">rôles</span>
        </x-slot>
        @can('roles.create')
            <a href="{{route('roles.create')}}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter un rôle
            </a>
        @endcan
    </x-title>

    <div class="mt-10 -mb-5">
        @foreach($roles as $role)
            <div class="mb-5">
                <div class="p-10 bg-white rounded shadow">
                    <span class="inline-block py-1 px-2 bg-blue-50 text-xs text-blue-500 rounded-full">{{$role->name}}</span>
                    <div class="my-4">
                        <h3 class="mb-2 font-medium">Liste des permissions</h3>
                        <p class="text-sm text-gray-500">
                            @if($role->permissions->count() == 0)
                                Aucune permission
                            @else
                                {{$role->permissions->implode('name', ', ')}}
                            @endif
                        </p>
                    </div>
                    <div>
                        <h3 class="font-medium mb-2">Liste des membres</h3>
                        @foreach($role->users as $user)
                            <p class="text-sm text-gray-500">{{$user->email}}</p>
                        @endforeach
                    </div>
                    <div class="mt-5 flex">
                        @can('roles.edit')
                            <a class="border rounded hover:bg-gray-50 font-semibold py-1 px-2" href="{{route('roles.edit', $role)}}">✏</a>
                        @endcan

                        @can('roles.delete')
                            <form action="{{route('roles.destroy', $role)}}" method="POST">
                                @csrf @method('DELETE')
                                <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-2">❌</button>
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</x-layout>
