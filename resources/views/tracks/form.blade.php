<label for="name" class="block mt-4">Nom du titre:</label>
<input value="{{$track?->name ?? old('name')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('name') border-2 border-red-500 @enderror"
       type="text" name="name" required>
@error('name')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

<label for="composer" class="block mt-4">Nom du compositeur:</label>
<input value="{{$track?->composer ?? old('composer')}}"
       class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('composer') border-2 border-red-500 @enderror"
       type="text" name="composer">
@error('composer')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

<label for="album_id" class="block mt-4">Sélectionner l'album:</label>
<select class="w-full p-4 text-xs font-semibold bg-blueGray-50 rounded outline-none appearance-none" name="album_id" required>
    <option value="0">Aucun album sélectionné</option>
    @foreach($albums as $album)
        <option value="{{$album->id}}" @if($track?->album_id == $album->id) selected @endif>{{$album->title}}</option>
    @endforeach
</select>

<label for="media_type_id" class="block mt-4">Sélectionner le type de média:</label>
<select class="w-full p-4 text-xs font-semibold bg-blueGray-50 rounded outline-none appearance-none" name="media_type_id" required>
    <option value="0">Aucun type sélectionné</option>
    @foreach($mediaTypes as $type)
        <option value="{{$type->id}}" @if($track?->media_type_id == $type->id) selected @endif>{{$type->name}}</option>
    @endforeach
</select>

<label for="genre_id" class="block mt-4">Sélectionner le genre:</label>
<select class="w-full p-4 text-xs font-semibold bg-blueGray-50 rounded outline-none appearance-none" name="genre_id" required>
    <option value="0">Aucun genre sélectionné</option>
    @foreach($genres as $genre)
        <option value="{{$genre->id}}" @if($track?->genre_id == $genre->id) selected @endif>{{$genre->name}}</option>
    @endforeach
</select>

<div class="flex -mx-3 mt-4">
    <div class="flex-col w-1/2 px-3">
        <label for="milliseconds" class="block">Durée (en secondes):</label>
        <div class="flex w-full mb-4 px-4 bg-blueGray-50 rounded">
            <input value="{{$track?->toSeconds()}}" name="milliseconds" class="w-full py-4 text-xs placeholder-blueGray-400 font-semibold leading-none bg-blueGray-50 outline-none" type="number" min="1">
        </div>
    </div>
    <div class="flex-col w-1/2 px-3">
        <label for="unit_price" class="block">Prix à l'unité:</label>
        <div class="flex w-full mb-4 px-4 bg-blueGray-50 rounded">
            <input value="{{$track?->unit_price ?? old('unit_price')}}" name="unit_price" class="w-full py-4 text-xs placeholder-blueGray-400 font-semibold leading-none bg-blueGray-50 outline-none" type="number" min="0" step=".01">
        </div>
    </div>
</div>

<button class="block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
