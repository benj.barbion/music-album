<table>
    <thead>
        <tr>
            <th>Titre</th>
            <th>Artiste</th>
            <th>Nombre titres</th>
            <th>Créé le</th>
            <th>Modifé le</th>
        </tr>
    </thead>
    <tbody>
        @foreach($albums as $album)
            <tr>
                <td>{{ $album->title }}</td>
                <td>{{ $album->artist?->name ?? 'Artiste inconnu' }}</td>
                <td>{{ $album->tracks_count }}</td>
                <td>{{ $album->created_at }}</td>
                <td>{{ $album->updated_at }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
