<x-layout>
    {{ Breadcrumbs::render() }}
    <x-title>
        <x-slot name="pageHeading">
            Création d'un <span class="text-blue-600">employé</span>
        </x-slot>
        <a href="{{ route('employees.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Liste des employés
        </a>
    </x-title>
    <section class="pt-10">
        <div class="container px-4 mx-auto">
            <div class="flex max-w-lg mx-auto flex-col">
                <div class="p-8 bg-white rounded shadow">
                    <div class=" text-center">
                        <span class="text-sm text-blueGray-400 content-center">Employés</span>
                        <h4 class="mb-6 text-3xl">Créer un employé</h4>
                    </div>
                    <form method="POST" action="{{route('employees.store')}}" role="form" enctype="multipart/form-data">
                        @csrf
                        @include('employees.form')
                        <button type="submit" class="block mt-4 w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Ajouter l'employé</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-layout>
