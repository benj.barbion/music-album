<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Track;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:cart');
    }

    public function index(Request $request){
        if ($request->has('cancel')){
            $request->session()->forget('customer_id');
            Cart::destroy();
        }

        if ($request->session()->missing('customer_id'))
            return view('cart.create-step-one')
                ->with('customers', Customer::orderBy('lastname')->orderBy('firstname')->get());

        return view('cart.create-step-two');
    }

    public function addToCart(Track $track){
        Cart::add($track, 1,  ['artiste' => $track->album?->artist?->name ?? 'Artist inconnu']);
        return back()->with('success', 'Ajout au panier avec succès !');
    }

    public function deleteFromCart($id){
        Cart::remove($id);
        return back()->with('success', 'Suppression de l\'article avec succès !');
    }

    public function update(Request $request, $id){
        $request->validate(['quantity' => 'required|integer|min:1']);

        $result = Cart::search(function ($cartItem, $rowId) use ($id) { return $rowId === $id; });
        if ($result->count() == 0)
            abort(404);

        Cart::update($id, $request->input('quantity'));

        return back()->with('success', 'Quantité modifiée avec succès.');
    }

    public function createStepOneStore(Request $request){
        $request->validate(['customer_id' => 'required']);
        $request->session()->put('customer_id', $request->input('customer_id'));
        return redirect()->route('cart.index');
    }

    public function store(Request $request){
        $invoice = new Invoice;
        $invoice->fill([
            'customer_id' => $request->session()->get('customer_id'),
            'invoice_date' => Carbon::now(),
            'total' => Cart::subtotal()
        ]);
        $invoice->save();

        $tracks = array_map(fn ($track) => ['track_id' => $track['id'], 'unit_price' => $track['price'], 'quantity' => $track['qty']], Cart::content()->toArray());
        $invoice->invoicesItems()->createMany($tracks);

        $request->session()->forget('customer_id');
        Cart::destroy();

        return redirect()->route('cart.success');
    }

    public function success(){
        return view('cart.success');
    }
}
