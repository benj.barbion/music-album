<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function playlistTracks() {
        return $this->hasMany(PlaylistTrack::class, 'playlist_id');
    }

    public function tracks() {
        return $this
            ->hasManyThrough(Track::class, PlaylistTrack::class, 'playlist_id', 'id', 'id', 'track_id')
            ->orderBy('playlist_track.created_at', 'desc');
    }

    public static function boot() {
        parent::boot();
        // we could use onDeleteCascade if we want to
        static::deleting(fn($playlist) => $playlist->playlistTracks()->delete());
    }
}
