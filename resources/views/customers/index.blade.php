<x-layout>
    {{ Breadcrumbs::render() }}

    <x-search title="des clients" searchBy="Cherchez par nom, prénom, email, ville ou encore société." />

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">clients</span>
        </x-slot>
        @can('customers.create')
            <a href="{{ route('customers.create') }}" class="cursor-pointer px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button ">
                Ajouter
            </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">@sortablelink('lastname', 'Nom')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('firstname', 'Prénom')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('email', 'Email')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('phone', 'Téléphone')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('city', 'Ville')</th>
                <th scope="col" class="px-6 py-3">@sortablelink('country', 'Pays')</th>
                <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $customer)
                <tr class="bg-white border-b">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{ $customer->lastname }}</th>
                    <td class="px-6 py-4">{{ $customer->firstname }}</td>
                    <td class="px-6 py-4">{{ $customer->email }}</td>
                    <td class="px-6 py-4">{{ $customer->phone ?? '-' }}</td>
                    <td class="px-6 py-4">{{ $customer->city ?? '-' }}</td>
                    <td class="px-6 py-4">{{ $customer->country ?? '-' }}</td>
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            @unless($customer->trashed())
                                @can('invoices.show')
                                    <a class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('invoices.index', ['customer' => $customer->id])}}">🧾</a>
                                @endcan

                                @can('customers.edit')
                                    <a class="border rounded ml-1 hover:bg-gray-50 font-semibold py-1 px-2" href="{{route('customers.edit', $customer)}}">✏</a>
                                @endcan

                                @can('customers.delete')
                                    <form action="{{route('customers.destroy', $customer)}}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">❌</button>
                                    </form>
                                @endcan
                            @else
                                <form action="{{ route('customers.restore', $customer) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                                </form>
                            @endunless
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($customers->hasPages())
        <div class="mt-10">
            {!! $customers->appends(\Request::except('page'))->render() !!}
        </div>
    @endif
</x-layout>
