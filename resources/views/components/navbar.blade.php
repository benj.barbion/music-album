<section>
    <div class="container px-4 mx-auto">
        <nav class="flex justify-between items-center py-6">
            <a class="text-3xl font-semibold leading-none" href="{{route('index')}}">
                <img src="{{asset('/images/logo.png')}}" alt="" width="auto">
            </a>
            <div class="lg:hidden">
                <button class="navbar-burger flex items-center py-2 px-3 text-blue-600 hover:text-blue-700 rounded border border-blue-200 hover:border-blue-300">
                    <svg class="fill-current h-4 w-4" viewbox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Menu mobile</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                    </svg>
                </button>
            </div>
            <ul class="hidden lg:flex lg:items-center lg:w-auto lg:space-x-12">
                <li><a class="text-sm text-blueGray-400 hover:text-blueGray-500" href="{{route('index')}}">Accueil</a></li>
                <li><a class="text-sm text-blueGray-400 hover:text-blueGray-500" href="{{route('albums.index')}}">Albums</a></li>
                <li><a class="text-sm text-blueGray-400 hover:text-blueGray-500" href="{{route('playlists.index')}}">Playlists</a></li>
                <li><a class="text-sm text-blueGray-400 hover:text-blueGray-500" href="{{route('artists.index')}}">Artistes</a></li>
                <li><a class="text-sm text-blueGray-400 hover:text-blueGray-500" href="{{route('tracks.index')}}">Titres</a></li>
            </ul>
            <div class="flex hidden lg:block">
                @auth
                    @can('cart')
                        <a href="{{route('cart.index')}}" class="mx-6 align-items-center">
                            <svg class="inline-block -mt-1" width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.1159 8.72461H2.50427C1.99709 8.72461 1.58594 9.12704 1.58594 9.62346V21.3085C1.58594 21.8049 1.99709 22.2074 2.50427 22.2074H18.1159C18.6231 22.2074 19.0342 21.8049 19.0342 21.3085V9.62346C19.0342 9.12704 18.6231 8.72461 18.1159 8.72461Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M6.34473 6.34469V4.95676C6.34473 3.85246 6.76252 2.79338 7.5062 2.01252C8.24988 1.23165 9.25852 0.792969 10.3102 0.792969C11.362 0.792969 12.3706 1.23165 13.1143 2.01252C13.858 2.79338 14.2758 3.85246 14.2758 4.95676V6.34469" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                            <span class="inline-block w-6 h-6 text-center text-white bg-blue-500 rounded-full font-semibold">{{Cart::count()}}</span>
                        </a>
                    @endcan
                    <button id="dropdownInformationButton" data-dropdown-toggle="dropdownInformation" class="px-3 py-2 text-xs text-blue-600 hover:text-blue-700 font-semibold leading-none border border-blue-200 hover:border-blue-300 rounded text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="button">
                        {{auth()->user()->name}} <svg class="w-4 h-4 ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>
                    </button>
                    <div id="dropdownInformation" class="z-10 hidden bg-white divide-y divide-gray-100 rounded shadow-2xl w-44 dark:bg-gray-700 dark:divide-gray-600 px-4 py-2">
                        <div class="px-4 py-3 text-sm text-gray-700 dark:text-white">
                            <div>Bienvenue !</div>
                            <div class="font-medium truncate">{{auth()->user()->email}}</div>
                        </div>
                        <div class="py-1">
                            <a href="{{route('users.account')}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Mon compte</a>
                        </div>
                        @canany(['roles.show', 'invoices.show', 'customers.show', 'employees.show'])
                            <ul class="py-1 text-sm text-gray-700" aria-labelledby="dropdownInformationButton">
                                @can('roles.show')
                                    <li><a href="{{route('roles.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des rôles</a></li>
                                @endcan
                                @can('invoices.show')
                                    <li><a href="{{route('invoices.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des commandes</a></li>
                                @endcan
                                @can('customers.show')
                                    <li><a href="{{route('customers.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des clients</a></li>
                                @endcan
                                @can('employees.show')
                                    <li><a href="{{route('employees.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des employés</a></li>
                                @endcan
                            </ul>
                        @endcan
                        <div class="py-1">
                            <a href="{{route('logout')}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Déconnexion</a>
                        </div>
                    </div>
                @else
                    <a class="mr-2 px-4 py-3 text-xs text-blue-600 hover:text-blue-700 font-semibold leading-none border border-blue-200 hover:border-blue-300 rounded" href="{{route('login')}}">Connexion</a>
                    <a class="px-4 py-3 text-xs font-semibold leading-none bg-blue-600 hover:bg-blue-700 text-white rounded" href="{{route('register')}}">Inscription</a>
                @endauth
            </div>
        </nav>
    </div>
    <div class="hidden navbar-menu fixed top-0 left-0 bottom-0 w-5/6 max-w-sm z-50">
        <div class="navbar-backdrop fixed inset-0 bg-blueGray-800 opacity-25"></div>
        <nav class="relative flex flex-col py-6 px-6 w-full h-full bg-white border-r overflow-y-auto">
            <div class="flex items-center mb-8">
                <a class="mr-auto text-3xl font-semibold leading-none" href="{{route('index')}}">
                    <img class="h-10" src="{{asset('/images/logo.png')}}" alt="" width="auto">
                </a>
                <button class="navbar-close">
                    <svg class="h-6 w-6 text-blueGray-400 cursor-pointer hover:text-blueGray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                    </svg>
                </button>
            </div>
            <div>
                <ul>
                    <li class="mb-1"><a class="block p-4 text-sm text-blueGray-600 hover:bg-blue-50 hover:text-blue-700" href="{{route('albums.index')}}">Albums</a></li>
                    <li class="mb-1"><a class="block p-4 text-sm text-blueGray-600 hover:bg-blue-50 hover:text-blue-700" href="{{route('playlists.index')}}">Playlists</a></li>
                    <li class="mb-1"><a class="block p-4 text-sm text-blueGray-600 hover:bg-blue-50 hover:text-blue-700" href="{{route('artists.index')}}">Artistes</a></li>
                    <li class="mb-1"><a class="block p-4 text-sm text-blueGray-600 hover:bg-blue-50 hover:text-blue-700" href="{{route('tracks.index')}}">Titres</a></li>
                </ul>
                <div class="mt-4 pt-6 border-t border-blueGray-100">
                    @auth
                        Bienvenue {{auth()->user()->name}}
                        <ul class="py-1 text-sm text-gray-700 mt-5" aria-labelledby="dropdownInformationButton">
                            <li><a href="{{route('users.account')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Mon compte</a></li>
                        @canany(['roles.show', 'invoices.show', 'customers.show', 'employees.show'])
                                @can('roles.show')
                                    <li><a href="{{route('roles.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des rôles</a></li>
                                @endcan
                                @can('invoices.show')
                                    <li><a href="{{route('invoices.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des commandes</a></li>
                                @endcan
                                @can('customers.show')
                                    <li><a href="{{route('customers.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des clients</a></li>
                                @endcan
                                @can('employees.show')
                                    <li><a href="{{route('employees.index')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Gestion des employés</a></li>
                                @endcan
                        @endcan
                            <li><a href="{{route('logout')}}" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Déconnexion</a></li>
                        </ul>
                    @else
                        <a class="block px-4 py-3 mb-3 text-xs text-center font-semibold leading-none bg-blue-600 hover:bg-blue-700 text-white rounded" href="{{route('register')}}">Inscription</a>
                        <a class="block px-4 py-3 mb-2 text-xs text-center text-blue-600 hover:text-blue-700 font-semibold leading-none border border-blue-200 hover:border-blue-300 rounded" href="{{route('login')}}">Connexion</a>
                    @endauth
                </div>
            </div>
        </nav>
    </div>
</section>
