<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:roles.edit')->only(['edit', 'update']);
        $this->middleware('permission:roles.delete')->only('destroy');
        $this->middleware('permission:roles.create')->only(['create', 'store']);
        $this->middleware('permission:roles.show')->only('index');
    }

    public function index(){
        $roles = Role::where('name', '!=', 'super admin')->with(['permissions:id,name', 'users:id,email'])->get();
        return view('roles.index', ['roles' => $roles]);
    }

    public function create(){
        return view('roles.create', ['role' => new Role]);
    }

    public function store(Request $request){
        $formFields = $request->validate([
            'name' => ['required', 'min:2', Rule::unique('roles')]
        ]);

        Role::create($formFields);

        return back()->with('success', 'Modèle inséré avec succès !');
    }

    public function edit(Role $role){
        $perms = \Spatie\Permission\Models\Permission::get();
        return view('roles.edit', ['role' => $role, 'perms' => $perms]);
    }

    public function update(Request $request, Role $role){
        $request->validate([
            'name' => 'required|min:2',
            'permissions.*' => Rule::exists('permissions', 'id')
        ]);

        if ($request->input('name') != $role->name)
            $request->validate(['name' => 'unique:roles']);

        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permissions'));

        return back()->with('success', 'Mise à jour avec succès !');
    }

    public function destroy(Request $request, Role $role){
        $role->delete();
        return back()->with('success', 'Rôle supprimé avec succès !');
    }
}
