<?php

namespace App\Exports;

use App\Models\Album;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithTitle;

class AlbumExport implements FromView, ShouldAutoSize, WithColumnWidths, WithTitle
{
    use Exportable;

    private Album $album;

    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    public function view(): View
    {
        $albums = [];
        $albums[] = $this->album;

        return view('exports.albums', ['albums' => $albums]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 55,
            'B' => 45,
        ];
    }

    public function title(): string
    {
        return "Album ".$this->album->title;
    }
}
