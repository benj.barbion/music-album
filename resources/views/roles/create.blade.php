<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Création d'un <span class="text-blue-600">rôle</span>
        </x-slot>
        <a href="{{ route('roles.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour aux rôles
        </a>
    </x-title>

    <div class="flex max-w-lg mx-auto flex-col pt-10">
        <div class="p-8 bg-white rounded shadow">
            <div class=" text-center">
                <span class="text-sm text-blueGray-400 content-center">Rôles</span>
                <h4 class="mb-6 text-3xl">Créer un rôle</h4>
            </div>
            <form method="POST" action="{{ route('roles.store') }}" role="form" enctype="multipart/form-data">
                @csrf
                <input value="{{$role->name}}" class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('name') border-2 border-red-500 @enderror" type="text" name="name" placeholder="Nom du rôle" required>
                @error('name')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror
                <button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
            </form>
        </div>
    </div>
</x-layout>
