<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Employee extends Model
{
    use HasFactory, Sortable, SoftDeletes;

    protected $fillable = ['firstname', 'lastname',  'company',  'address', 'city', 'state', 'country', 'postalcode', 'phone', 'fax', 'email', 'employee_id'];
    protected $sortable = ['id', 'firstname', 'lastname', 'email', 'phone', 'city', 'country', 'title'];
    protected $with = ['user'];

    public function customers(): HasMany {
        return $this->hasMany(Customer::class, 'employee_id');
    }

    public function employee(): BelongsTo {
        return $this->belongsTo(Employee::class, 'reportsto');
    }

    public function user(): BelongsTo {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['search'] ?? false)
            return $query->where('lastname', 'like', '%' . $filter['search'] . '%')
                ->orWhere('firstname', 'like', '%' . $filter['search'] . '%')
                ->orWhere('title', 'like', '%' . $filter['search'] . '%')
                ->orWhere('city', 'like', '%' . $filter['search'] . '%')
                ->orWhere('postalcode', '=', $filter['search'])
                ->orWhere('email', 'like', '%' . $filter['search'] . '%');
        return $query;
    }

    public static function boot() {
        parent::boot();
        static::deleting(fn($employee) => $employee->user()->delete());
        static::restoring(fn($employee) => $employee->user()->restore());
    }
}
