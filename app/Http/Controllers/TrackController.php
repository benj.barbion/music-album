<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Genre;
use App\Models\MediaType;
use App\Models\Track;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TrackController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:tracks.edit')->only(['edit', 'update']);
        $this->middleware('permission:tracks.delete')->only(['destroy', 'restore']);
        $this->middleware('permission:tracks.create')->only(['create', 'store']);
    }

    public function index(){
        $tracks = auth()->user()->can('tracks.delete') ? Track::withTrashed() : Track::withoutTrashed();

        $tracks = $tracks->sortable()
            ->with(['album.artist:id,name'])
            ->withSum('invoiceItems', 'quantity')
            ->orderBy('tracks.updated_at', 'desc')
            ->filter(\request(['search']))
            ->paginate(15);

        return view('tracks.index')->with('tracks', $tracks);
    }

    public function restore($idTrack){
        $track = Track::onlyTrashed()->findOrFail($idTrack);
        $track->restore();
        return back()->with('success', 'Titre restauré avec succès.');
    }

    public function create(Request $request){
        $request->validate(['album' => 'integer|min:0']);

        $track = new Track;
        if (request()->has('album'))
            $track->album_id = request()->input('album');

        return view('tracks.create')
            ->with('track', $track)
            ->with('albums', Album::orderedAlbums())
            ->with('genres', Genre::orderBy('name', 'asc')->get(['id', 'name']))
            ->with('mediaTypes', MediaType::orderBy('name', 'asc')->get(['id', 'name']));
    }

    public function edit(Track $track){
        return view('tracks.edit')
            ->with('track', $track)
            ->with('albums', Album::orderedAlbums())
            ->with('genres', Genre::orderBy('name', 'asc')->get(['id', 'name']))
            ->with('mediaTypes', MediaType::orderBy('name', 'asc')->get(['id', 'name']));
    }

    public function update(Request $request, Track $track){
        $formFields = $request->validate($this->verificationRules($request));

        if ($request->input('name') != $track->name)
            $request->validate(['name' => Rule::unique('tracks', 'name')->where(function($query) use ($formFields) {
                return $query->where('album_id', $formFields['album_id']);
            })]);

        $formFields['milliseconds'] *= 1000;
        $track->update($formFields);
        return back()->with('success', 'Modification réussie!');
    }

    public function store(Request $request){
        $formFields = $request->validate($this->verificationRules($request));

        $request->validate(['name' => Rule::unique('tracks', 'name')->where(function($query) use ($formFields) {
            return $query->where('album_id', $formFields['album_id']);
        })]);

        $formFields['milliseconds'] *= 1000;
        Track::create($formFields);
        return back()->with('success', 'Ajout du titre avec succès !');
    }

    public function destroy(Track $track){
        $track->delete();
        return back()->with('success', 'Suppression de l\'album avec succès !');
    }

    private function verificationRules(Request $request) : array {
        return [
            'media_type_id' => ['required', Rule::exists('media_types', 'id')],
            'genre_id' => ['required', Rule::exists('genres', 'id')],
            'milliseconds' => ['required', 'integer', 'min:1'],
            'unit_price' => ['required', 'numeric', 'min:0'],
            'name' => ['required', 'min:2'],
            'album_id' => ['required', Rule::exists('albums', 'id')]
        ];
    }
}
