<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Gestion d'un <span class="text-blue-600">employé</span>
        </x-slot>
        @if(is_null($employee->user))
            <form action="{{ route('employees.store.account', $employee->id) }}" method="POST" class="inline m-0">
                @csrf
                <button class="mr-1 cursor-pointer px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                    Créer un compte utilisateur
                </button>
            </form>
        @endif
        <a href="{{ route('employees.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Liste des employés
        </a>
    </x-title>

    <div class="grid grid-cols-1 lg:grid-cols-1 gap-6 mt-10 max-w-3xl mx-auto">
        <div class="p-6 bg-white rounded shadow">
            <div class=" text-center">
                <span class="text-sm text-blueGray-400 content-center">Employés</span>
                <h4 class="mb-6 text-3xl">Modifier un employé</h4>
            </div>
            <form method="POST" action="{{ route('employees.update', $employee->id) }}" role="form" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                @include('employees.form')
                @if(!is_null($employee->user))
                    <label for="country" class="block mt-4">Rôles de l'utilisateur:</label>
                    <select class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none h-32" name="roles[]" multiple>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" @if($employee->user->hasRole($role)) selected @endif>{{$role->name}}</option>
                        @endforeach
                    </select>
                    @error('artist')<p class="text-red-500 text-xs">{{$message}}</p>@enderror
                @endif
                <button type="submit" class="block mt-5 w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Modifier les informations</button>
            </form>
        </div>
    </div>
</x-layout>
