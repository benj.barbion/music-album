<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:employees.show')->only('index');
        $this->middleware('permission:employees.edit')->only(['edit', 'update', 'storeAccount']);
        $this->middleware('permission:employees.delete')->only(['destroy', 'restore']);
        $this->middleware('permission:employees.create')->only(['create', 'store']);
    }

    public function index(){
        $employees = auth()->user()->can('employees.delete') ? Employee::withTrashed() : Employee::withoutTrashed();
        $employees = $employees->filter(\request()->input())->sortable()->latest()->paginate(10);
        return view('employees.index', ['employees' => $employees]);
    }

    public function create(){
        return view('employees.create', ['employee' => new Employee]);
    }

    public function edit(Employee $employee){
        $roles = Role::where('name', '!=', 'super admin')->get();
        return view('employees.edit', ['employee' => $employee, 'roles' => $roles]);
    }

    public function destroy(Employee $employee){
        $employee->delete();
        return back()->with('success', 'Suppression de l\'employé avec succès !');
    }

    public function restore($employeeId){
        $employee = Employee::onlyTrashed()->findOrFail($employeeId);
        $employee->restore();
        return back()->with('success', 'Employé restauré avec succès.');
    }

    public function update(Request $request, Employee $employee){
        $formFields = [
            'lastname' => ['required', 'min:2'],
            'firstname' => ['required', 'min:2'],
            'roles.*' => Rule::exists('roles', 'id')
        ];

        if ($request->input('email') != $employee->email)
            $formFields['email'] = 'email:rfc,dns|unique:employees';

        $request->validate($formFields);

        if($employee->user()->exists())
            $employee->user->syncRoles($request->input('roles'));

        $employee->updateOrFail($request->input());

        return back()->with('success', 'Modification réussie avec succès !');
    }

    public function store(Request $request){
        $request->validate([
            'lastname' => ['required', 'min:2'],
            'firstname' => ['required', 'min:2'],
            'email' => 'email:rfc,dns|unique:employees'
        ]);

        $fields = $request->input();
        $fields['reportsto'] = $request->user()->employee->id;

        Employee::create($fields);

        return back()->with('success', 'Création réussie avec succès !');
    }

    public function storeAccount(Request $request, Employee $employee){
        $password = Str::random(8);

        $user = User::firstOrCreate(['email' => $employee->email], [
            'name' => $employee->lastname,
            'email' => $employee->email,
            'password' => Hash::make($password)
        ]);

        $employee->user()->associate($user);
        $employee->save();

        return back()->with('success', 'Enregistrez bien le mot de passe : '.$password);
    }
}
