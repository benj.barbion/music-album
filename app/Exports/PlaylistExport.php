<?php

namespace App\Exports;

use App\Models\Playlist;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class PlaylistExport implements FromView, WithTitle, ShouldAutoSize
{
    private Playlist $playlist;

    public function __construct(Playlist $idPlaylist)
    {
        $this->playlist = $idPlaylist;
    }

    public function title(): string
    {
        return 'Playlist '.$this->playlist->name;
    }

    public function view(): View
    {
        return view('exports.tracks', ['tracks' => $this->playlist->tracks]);
    }
}
