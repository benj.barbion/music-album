<x-layout>
    <x-title>
        <x-slot name="pageHeading">
            Mon <span class="text-blue-600">compte</span>
        </x-slot>
        <a href="{{ route('index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Retour à l'accueil
        </a>
    </x-title>

    <section class="pt-10">
        <div class="container px-4 mx-auto">
            <div class="flex max-w-lg mx-auto flex-col">
                <div class="p-8 bg-white rounded shadow">
                    <div class=" text-center">
                        <span class="text-sm text-blueGray-400 content-center">Compte</span>
                        <h4 class="mb-6 text-3xl">Modifier le mot de passe</h4>
                    </div>
                    <form method="POST" action="{{route('users.update', auth()->user()->id)}}" role="form" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <label for="old_password" class="block mt-4">Mot de passe actuel:</label>
                        <input class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('old_password') border-2 border-red-500 @enderror"
                               type="password" name="old_password" required>
                        @error('old_password')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

                        <label for="password" class="block mt-4">Nouveau mot de passe:</label>
                        <input class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('password') border-2 border-red-500 @enderror"
                               type="password" name="password" required>
                        @error('password')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

                        <label for="password_confirmation" class="block mt-4">Répétez le mot de passe:</label>
                        <input class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none"
                               type="password" name="password_confirmation" required>

                        <button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-layout>
