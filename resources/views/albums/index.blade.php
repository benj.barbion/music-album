<x-layout>
    {{ Breadcrumbs::render() }}

    <x-search title="des albums" searchBy="Cherchez par le titre des albums ou le nom des artistes !" />

    <x-title>
        <x-slot name="pageHeading">
            Liste des <span class="text-blue-600">albums</span>
        </x-slot>
        <a href="{{ route('albums.export') }}" class="cursor-pointer px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Exporter <i class="ml-2 fa fa-file-excel-o" aria-hidden="true"></i>
        </a>
        @can('albums.create')
        <a href="{{ route('albums.create') }}" class="ml-2 cursor-pointer px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
            Ajouter
        </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('title', 'Titre de l\'album')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('artist.name', 'Artiste')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('tracks_count', 'Chansons')
                </th>
                <th scope="col" class="px-6 py-3">
                    @sortablelink('updated_at', 'Modification')
                </th>
                <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Actions</span>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($albums as $album)
            <tr class="bg-white border-b">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap flex">
                    <img class="w-12 rounded-full hidden lg:flex" alt="pochette" src="{{$album->pochette ? asset('/storage/'.$album->pochette) : asset('/images/no-image.png')}}">
                    <div class="pl-4">
                        <p class="font-semibold">{{$album->title}}</p>
                        <a class="text-blueGray-400" href="{{ route('artists.index', ['search' => $album->artist?->name]) }}">{{$album->artist?->name ?? 'Artiste inconnu'}}</a>
                    </div>
                </th>
                <td class="px-6 py-4">
                    {{$album->artist?->name}}
                </td>
                <td class="px-6 py-4">
                    {{$album->tracks_count}}
                </td>
                <td class="px-6 py-4">
                    {{$album->updated_at}}
                </td>
                <td class="px-6 py-4">
                    <div class="flex justify-end">
                        @unless($album->trashed())
                            @can('cart')
                                <form action="{{route('albums.cart', $album)}}" method="POST">
                                    @csrf <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🛒</button>
                                </form>
                            @endcan
                            <a class="ml-1 border hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('albums.show', $album->id)}}">🔎</a>
                            @can('albums.edit')
                                <a class="ml-1 border hover:bg-gray-50 font-semibold py-1 px-2 rounded" href="{{route('albums.edit', $album->id)}}">✏</a>
                            @endcan
                            @can('albums.delete')
                                <form action="{{route('albums.destroy', $album->id)}}" method="POST">
                                    @csrf @method('DELETE') <button type="submit" class="ml-1 border hover:bg-gray-50 font-semibold py-1 px-2 rounded">❌</button>
                                </form>
                            @endcan
                        @else
                            <form action="{{ route('albums.restore', $album->id) }}" method="POST">
                                @csrf <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                            </form>
                        @endunless
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if($albums->hasPages())
        <div class="mt-10">
            {!! $albums->appends(\Request::except('page'))->render() !!}
        </div>
    @endif
</x-layout>
