<?php

return [
    ['to' => 5, 'result' => 'Aucune'],
    ['to' => 40, 'result' => '❤'],
    ['to' => 100, 'result' => '❤❤'],
    ['to' => 1000, 'result' => '❤❤❤'],
    ['to' => 10000, 'result' => '❤❤❤❤']
];
