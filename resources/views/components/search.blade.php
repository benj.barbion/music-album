<section class="py-7 pb-10 bg-blueGray-50">
    <div class="container px-4 mx-auto">
        <div class="max-w-xl mx-auto text-center">
            <h2 class="mb-4 text-3xl lg:text-4xl font-bold font-heading">
                <span>La recherche</span>
                <span class="text-blue-600">{{$title}}</span>
                <span>est ce qu'il y a de plus beau.</span>
            </h2>
            <p class="mb-8 text-blueGray-400">{{$searchBy}}</p>
            <form action="{{Request::url()}}" method="get">
                <div class="flex flex-wrap max-w-lg mx-auto">
                    <div class="flex w-full md:w-2/3 px-3 mb-3 md:mb-0 md:mr-6 bg-blueGray-100 rounded">
                        <input name="search" class="w-full pl-3 py-4 text-xs text-blueGray-400 font-semibold leading-none bg-blueGray-100 outline-none" type="text" placeholder="Tappez votre recherche">
                    </div>
                    <button class="w-full md:w-auto py-4 px-8 text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded" type="submit">Rechercher</button>
                </div>
            </form>
        </div>
    </div>
</section>
