<!--{{ old('username') }}-->

<input value="{{$artist->name}}" class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none @error('name') border-2 border-red-500 @enderror" type="text" name="name" placeholder="Nom de l'artiste" required>
@error('name')<p class="text-red-500 text-xs italic">{{$message}}</p>@enderror

<span class="font-medium block mt-4">Pochette:</span>
<input class="appearance-none w-full p-4 text-xs font-semibold leading-none bg-blueGray-50 rounded outline-none" type="file" name="pochette">

<button class="mt-4 block w-full p-4 text-center text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded">Valider les informations</button>
