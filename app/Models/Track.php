<?php

namespace App\Models;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Track extends Model implements Buyable
{
    use HasFactory, SoftDeletes, Sortable;

    protected $sortable = ['name', 'milliseconds', 'unit_price', 'genre.name'];
    protected $sortableAs = ['invoice_items_sum_quantity'];
    protected $fillable = ['name', 'album_id', 'media_type_id', 'genre_id', 'composer', 'milliseconds', 'bytes', 'unit_price'];
    protected $with = ['mediaType', 'genre']; // eager loading by default - optimize request !

    public function album(): BelongsTo {
        return $this->belongsTo(Album::class, 'album_id')->withTrashed();
    }

    public function genre() {
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function mediaType() {
        return $this->belongsTo(MediaType::class, 'media_type_id');
    }

    public function invoiceItems() {
        return $this->hasMany(InvoiceItem::class, 'track_id');
    }

    public function getArtistAttribute()
    {
        return $this->album?->artist?->name ?? 'Artiste inconnu';
    }

    public function getMinutesAttribute()
    {
        return gmdate('H:i:s', $this->milliseconds/1000);
    }

    public function toSeconds()
    {
        return floor($this->milliseconds/1000);
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->unit_price;
    }

    public function getPopularityAttribute() {
        $popularity = config('popularity');

        foreach ($popularity as $current)
            if ($this->invoice_items_sum_quantity < $current['to'])
                return $current['result'];

        return 'Aucune';
    }

    public function artistSortable($query, $direction) {
        return $query->leftjoin('albums', 'tracks.album_id', '=', 'albums.id')
            ->leftjoin('artists', 'albums.artist_id', '=', 'artists.id')
            ->orderBy('artists.name', $direction);
    }

    public function scopePopular($query, int $total = 5) {
        return $query->withSum('invoiceItems as quantity_sum', 'quantity')->orderBy('quantity_sum', 'desc')->take($total);
    }

    public function scopeFilter($query, array $filter) {
        if ($filter['search'] ?? false) {
            return $query->where('name', 'like', '%' . request('search') . '%')
                ->orWhereHas('genre', fn($genre) => $genre->where('name', 'like', '%' . request('search') . '%'))
                ->orWhereHas('album.artist', fn($artist) => $artist->where('name', 'like', '%' . request('search') . '%'));
        }

        return $query;
    }
}
