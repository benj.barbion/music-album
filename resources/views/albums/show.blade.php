<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Album <span class="text-blue-600">{{$album->title}}</span>
        </x-slot>
        <a href="{{ route('albums.export.one', $album->id) }}" class="cursor-pointer px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Exporter <i class="ml-2 fa fa-file-excel-o" aria-hidden="true"></i>
        </a>
        @can('tracks.create')
            <a href="{{ route('tracks.create', ['album' => $album]) }}" class="ml-2 cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-green-500 hover:bg-green-600 border border-green-600 rounded-md shadow-button">
                Ajouter
            </a>
        @endcan
    </x-title>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-10">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">Titre</th>
                <th scope="col" class="px-6 py-3">Genre</th>
                <th scope="col" class="px-6 py-3">Durée</th>
                <th scope="col" class="px-6 py-3">Type média</th>
                <th scope="col" class="px-6 py-3">Prix</th>
                @canany(['cart', 'albums.edit', 'albums.delete'])
                    <th scope="col" class="px-6 py-3"><span class="sr-only">Actions</span></th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($tracks as $track)
            <tr class="bg-white border-b">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{$track->name}}</th>
                <td class="px-6 py-4">{{$track->genre->name}}</td>
                <td class="px-6 py-4">{{$track->minutes}}</td>
                <td class="px-6 py-4">{{$track->mediaType->name}}</td>
                <td class="px-6 py-4">{{$track->unit_price}}Є</td>
                @canany(['cart', 'albums.edit', 'albums.delete'])
                    <td class="px-6 py-4">
                        <div class="flex justify-end">
                            @unless($track->trashed())
                                @can('cart')
                                    <form action="{{route('cart.add', $track)}}" method="POST">
                                        @csrf <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">🛒</button>
                                    </form>
                                @endcan
                                @can('albums.edit')
                                    <a class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1" href="{{route('tracks.edit', $track)}}">✏</a>
                                @endcan
                                @can('albums.delete')
                                    <form action="{{route('tracks.destroy', $track)}}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded ml-1">❌</button>
                                    </form>
                                @endcan
                            @else
                                <form action="{{ route('tracks.restore', $track) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="border hover:bg-gray-50 font-semibold py-1 px-2 rounded">🔓</button>
                                </form>
                            @endunless
                        </div>
                    </td>
                @endcan
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($tracks->hasPages())
        <div class="mt-10">
            {{$tracks->links()}}
        </div>
    @endif
</x-layout>


