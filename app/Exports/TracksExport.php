<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithTitle;

class TracksExport implements FromView, ShouldAutoSize, WithColumnWidths, WithTitle
{
    use Exportable;

    private $tracks;

    public function __construct($tracks)
    {
        $this->tracks = $tracks;
    }

    public function view(): View
    {
        return view('exports.tracks', [
            'tracks' => $this->tracks
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 55,
            'B' => 30,
        ];
    }

    public function title(): string
    {
        return 'Liste des chansons';
    }
}
