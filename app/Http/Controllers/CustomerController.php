<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permission:customers.show')->only('index', 'show');
        $this->middleware('permission:customers.edit')->only(['edit', 'update']);
        $this->middleware('permission:customers.delete')->only(['destroy', 'restore']);
        $this->middleware('permission:customers.create')->only(['create', 'store']);
    }

    public function index(){
        $customers = auth()->user()->can('customers.delete') ? Customer::withTrashed() : Customer::withoutTrashed();
        $customers = $customers->filter(\request()->input())->sortable()->latest()->paginate(15);
        return view('customers.index', ['customers' => $customers]);
    }

    public function create(){
        return view('customers.create', ['customer' => new Customer]);
    }

    public function edit(Customer $customer){
        return view('customers.edit', ['customer' => $customer]);
    }

    public function update(Request $request, Customer $customer){
        $formFields = [
            'lastname' => ['required', 'min:2'],
            'firstname' => ['required', 'min:2']
        ];

        if ($request->input('email') != $customer->email)
            $formFields['email'] = 'email:rfc,dns|unique:customers';

        $request->validate($formFields);

        $customer->updateOrFail($request->input());

        return back()->with('success', 'Modification réussie avec succès !');
    }

    public function destroy(Customer $customer){
        $customer->delete();
        return back()->with('success', 'Suppression du client avec succès !');
    }

    public function restore($customerId){
        $customer = Customer::onlyTrashed()->findOrFail($customerId);
        $customer->restore();
        return back()->with('success', 'Client restauré avec succès.');
    }

    public function store(Request $request){
        $request->validate([
            'lastname' => ['required', 'min:2'],
            'firstname' => ['required', 'min:2'],
            'email' => 'email:rfc,dns|unique:customers'
        ]);

        $fields = $request->input();
        $fields['employee_id'] = $request->user()->id;

        Customer::create($fields);

        return back()->with('success', 'Création réussie avec succès !');
    }

    public function show(Customer $customer){
        return view('invoices.index', ['customer' => $customer]);
    }
}
