<x-layout>
    <div class="text-center">
        <div class="max-w-lg mx-auto mb-10">
            <span class="inline-block py-1 px-3 text-xs font-semibold bg-blue-100 text-blue-600 rounded-xl">Les albums</span>
            <h2 class="text-3xl md:text-4xl mt-2 mb-4 font-bold font-heading">Nos albums les plus populaires</h2>
            <p class="text-blueGray-400 leading-loose">@auth Profitez de notre magnifiiiiiiiique catalogue ! @else Connectez-vous pour voir le catalogue complet ! @endauth</p>
        </div>
        <div class="flex flex-wrap -mx-5">
            @foreach($albums as $album)
                <div class="w-1/2 lg:w-1/4 px-5 mb-12">
                    <img class="h-20 w-20 mx-auto rounded-full object-cover object-top" src="{{$album->pochette ? asset('/storage/'.$album->pochette) : asset('/images/no-image.png')}}" alt="">
                    <p class="mt-6 mb-2 text-xl"><a href="{{route('albums.show', $album)}}">{{$album->title}}</a></p>
                    <p class="text-blue-600"><a href="{{route('artists.index', ['artist' => $album->artist?->id])}}">{{$album->artist?->name ?? 'Artiste supprimé'}}</a></p>
                </div>
            @endforeach
        </div>
        @auth
            <a class="inline-block py-4 px-8 text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded" href="{{route('albums.index')}}">
                Voir tous les albums
            </a>
        @else
            <a class="inline-block py-4 px-8 text-xs text-white font-semibold leading-none bg-blue-600 hover:bg-blue-700 rounded" href="{{route('login')}}">
                Connectez-vous pour voir plus !
            </a>
        @endauth
    </div>
</x-layout>
