<table>
    <thead>
        <tr>
            <th>Titre</th>
            <th>Genre</th>
            <th>Durée</th>
            <th>Prix</th>
            <th>Album</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tracks as $pop)
            <tr>
                <td>{{ $pop->name }}</td>
                <td>{{ $pop->genre->name }}</td>
                <td>{{ $pop->minutes }}</td>
                <td>{{ $pop->unit_price }}Є</td>
                <td>{{ $pop->album?->title ?? 'Aucun album lié' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
