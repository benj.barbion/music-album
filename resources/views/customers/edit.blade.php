<x-layout>
    {{ Breadcrumbs::render() }}

    <x-title>
        <x-slot name="pageHeading">
            Modification d'un <span class="text-blue-600">client</span>
        </x-slot>
        <a href="{{ route('customers.index') }}" class="cursor-pointer w-full px-4 py-2 text-sm text-white font-medium bg-blue-500 hover:bg-blue-600 border border-blue-600 rounded-md shadow-button">
            Liste des clients
        </a>
    </x-title>

    <div class="flex max-w-lg mx-auto flex-col mt-10">
        <div class="p-8 bg-white rounded shadow">
            <div class=" text-center">
                <span class="text-sm text-blueGray-400 content-center">Clients</span>
                <h4 class="mb-6 text-3xl">Modifier un client</h4>
            </div>
            <form method="POST" action="{{ route('customers.update', $customer->id) }}" role="form" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                @include('customers.form')
            </form>
        </div>
    </div>
</x-layout>
